<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_quotation', function (Blueprint $table) {
            $table->unsignedBigInteger('id_cotizacion');
            $table->unsignedBigInteger('id_items');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id_cotizacion')->references('id')->on('quotation');
            $table->foreign('id_items')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_quotation');
    }
};

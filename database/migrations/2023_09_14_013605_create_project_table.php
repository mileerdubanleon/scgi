<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->decimal('price');
            $table->decimal('price_company');
            $table->decimal('price_material');
            $table->unsignedBigInteger('employee_name');
            $table->decimal('price_employee');
            $table->integer('state');
            $table->softDeletes();
            $table->timestamps();

            //Relaciones uno a muchos
            $table->foreign('employee_name')->references('id')->on('employee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
};

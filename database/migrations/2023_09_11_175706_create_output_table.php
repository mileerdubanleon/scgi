<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('output', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('name_inventory_output');
            $table->unsignedBigInteger('output_employee');
            $table->integer('quantity');
            $table->softDeletes();
            $table->timestamps();

            //Relaciones
            $table->foreign('name_inventory_output')->references('id')->on('inventory');

            //Relaciones
            $table->foreign('output_employee')->references('id')->on('employee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('output');
    }
};

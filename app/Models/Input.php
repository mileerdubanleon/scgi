<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Input extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];
    protected $table = 'input';
    protected $hidden = ['created_at', 'updated_at'];

    public function inventory() {
        return $this->belongsTo(Inventory::class, 'name_inventory'); // Asegúrate de que 'inventory_id' sea la clave foránea correcta
    }

}

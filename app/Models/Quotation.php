<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotation extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];
    protected $table = 'quotation';
    protected $hidden = ['created_at', 'updated_at'];

    public function details()
    {
        return $this->hasMany(DetailsQuotation::class, 'id_cotizacion', 'id');
    }

    // En el modelo Quotation
    // En el modelo Quotation
    public function items()
    {
        return $this->hasManyThrough(
            'App\Models\Items',
            'App\Models\DetailsQuotation',
            'id_cotizacion', // Clave foránea en DetailsQuotation
            'id', // Clave primaria en Items
            'id', // Clave primaria en Quotation
            'id_items' // Clave foránea en DetailsQuotation
        );
    }

    public function details_quotations()
    {
        return $this->hasMany(DetailsQuotation::class, 'id_cotizacion');
    }

    public function client(){
        return $this->belongsTo(Client::class, 'id_cliente');
    }

    public function detailsQuotation()
    {
        return $this->hasMany(DetailsQuotation::class, 'id_cotizacion');
    }



}

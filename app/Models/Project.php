<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = ['state']; // btn state
    protected $dates = ['deleted_at'];
    protected $table = 'project';
    protected $hidden = ['created_at', 'updated_at'];

    public function employee() {
        return $this->belongsTo(Employee::class, 'employee_name', 'id'); // Corregir el nombre de la columna
    }

}

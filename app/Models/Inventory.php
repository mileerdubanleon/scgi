<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    use SoftDeletes;

    protected $fillable = ['state']; // btn state
    protected $dates = ['deleted_at'];
    protected $table = 'inventory';
    protected $hidden = ['created_at', 'updated_at'];

    public function inputs() {
        return $this->hasMany(Input::class, 'name_inventory', 'id');
    }

    public function outputs() {
        return $this->hasMany(Output::class, 'name_inventory_output', 'id');
    }

    public function output(){
        return $this->hasMany(Output::class, 'name_inventory_output', 'id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotes extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];
    protected $table = 'quotes';
    protected $hidden = ['created_at', 'updated_at'];

    public function details(){
        return $this->hasMany(DetailsQuotes::class, 'details_quotes');
    }
    
    public function tableQuotes(){
        return $this->hasMany(TableQuotes::class, 'details_quotes', 'id');
    }

    public function pdfQuotes(){
        return $this->hasMany(TableQuotes::class, 'codigo', 'id');
    }

}

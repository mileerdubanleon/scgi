<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $fillable = ['state']; // btn state
    protected $dates = ['delete_at'];
    protected $table = 'employee';
    protected $hidden = ['created_at', 'updated_at'];

}

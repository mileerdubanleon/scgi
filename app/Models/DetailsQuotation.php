<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailsQuotation extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];
    protected $table = 'details_quotation';
    protected $hidden = ['created_at', 'updated_at'];

    public function item(){
        return $this->belongsTo('App\Models\Items', 'id_items');
    }

    public function quotation(){
        return $this->belongsTo(Quotation::class, 'id_cotizacion');
    }

    public function getClientData(){
        if ($this->quotation) {
            return $this->quotation->client;
        }

        return null;
    }
}

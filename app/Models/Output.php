<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Output extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];
    protected $table = 'output';
    protected $hidden = ['created_at', 'updated_at'];

    public function inventory() {
        return $this->belongsTo(Inventory::class, 'name_inventory_output'); // Asegúrate de que 'inventory_id' sea la clave foránea correcta
    }

    public function employee() {
        return $this->belongsTo(Employee::class, 'output_employee'); // 
    }

    public function inventories()
    {
        return $this->belongsTo(Inventory::class);
    }

}

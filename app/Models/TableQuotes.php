<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TableQuotes extends Model
{
    use SoftDeletes;

    protected $dates = ['delete_at'];
    protected $table = 'details_quotes';
    protected $hidden = ['created_at', 'updated_at'];

    public function quote()
    {
        return $this->belongsTo(DetailsQuotes::class, 'details_quotes', 'client_name');
    }

    public function detailsQuotes(){
        return $this->hasMany(DetailsQuotes::class, 'codigo', 'codigo');
    }

    public function tableQuotes() {
        return $this->belongsTo(DetailsQuotes::class, 'client_name');
    }
    

}

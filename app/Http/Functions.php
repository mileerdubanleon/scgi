<?php

// Key value From Json
function kvfj($json, $key){
    if($json == null):
        return null;
    else:
        $json = $json;
        $json = json_decode($json, true);
        if(array_key_exists($key, $json)):
            return $json[$key];
        else:
            return null;
        endif;
    endif;    
}

function getRoleUserArray($mode, $id){
    $roles = ['0' => 'Usuario normal', '1' => 'Administrador'];
    if(!is_null($mode)):
                return $roles;
        else:
                return $roles[$id];
    endif;
}

function getUserStatusArray($mode, $id){
    $status = ['0' => 'Registrado', '1' => 'Verificado', '100' => 'Baneado'];
    if(!is_null($mode)):
                return $status;
        else:
                return $status[$id];
    endif;
}

function user_permissions(){
    $p = [
        'dashboard' => [
            'icon' => '<i class="fas fa-home"></i>',
            'title' => 'Modulo Dashboard',
            'keys' => [
                'dashboard' => 'Puede ver dashboard.'
            ]
        ],

        'clients' => [
            'icon' => '<i class="fas fa-user-friends"></i>',
            'title' => 'Modulo Clientes',
            'keys' => [
                'clients' => 'Puede ver el listado de clientes.',
                'clientAdd' => 'Puede agregar clients.',
                'client_edit' => 'Puede editar clientes.',
                'client_delete' => 'Puede eliminar clientes.',
                ]
            ],
        'items' => [
            'icon' => '<i class="fas fa-window-restore"></i>',
            'title' => 'Modulo de cotizaciones',
            'keys' => [
                'items' => 'Puede crear una cotizacion',
                'mostrarItems' => 'Puede agregar items',
                'quotationsConsult' => 'Puede consultar cotizaciones',
                ]
            ],
        'inventario' => [
            'icon' => '<i class="fas fa-box"></i>',
            'title' => 'Modulo de inventario',
            'keys' => [
                'inventario' => 'Puede ver los productos',
                'inventario_btn' => 'Puede crear productos',
                'inventario_edit' => 'Puede editar productos',
                'inventario_delete' => 'Puede elimnar productos',
                ]
            ],
        'input' => [
            'icon' => '<i class="fas fa-outdent"></i>',
            'title' => 'Modulo de entradas',
            'keys' => [
                'input' => 'Puede ver las entradas',
                'inputAdd' => 'Puede crear una entrada',
                'input_delete' => 'Puede eliminar entradas',
                ]
            ],
        'output' => [
            'icon' => '<i class="fas fa-arrow-right"></i>',
            'title' => 'Modulo de salidas',
            'keys' => [
                'output' => 'Puede ver las salidas',
                'outputAdd' => 'Puede crear una salida',
                'output_deleted' => 'Puede eliminar salidas',
                'stadisticas' => 'Puede ver las estadísticas',
                ]
            ],
        'operative' => [
            'icon' => '<i class="fas fa-door-open"></i>',
            'title' => 'Modulo operativo',
            'keys' => [
                'operative' => 'Puede ver modulo operativo',
                'employee' => 'Puede ver empleados',
                'employee_add' => 'Puede agregar empleados',
                'employee_edit' => 'Puede editar empleados',
                'employee_delete' => 'Puede eliminar empleados',
                'project' => 'Puede ver los proyectos',
                'project_add' => 'Puede crear proyectos',
                'project_edit' => 'Puede editar los proyectos',
                'project_delete' => 'Puede eliminar los proyectos',
                ]
            ],
        'settings' => [
            'icon' => '<i class="fas fa-cogs"></i>',
            'title' => 'Modulo de ajustes del sistema',
            'keys' => [
                'settings' => 'Puede ver la informacion de la aplicación',
                'publicity' => 'Puede ver la publicidad de la aplicación',
                ]
            ],
        'users' => [
            'icon' => '<i class="fas fa-user-friends"></i>',
            'title' => 'Modulo de usuarios y permisos',
            'keys' => [
                'permission_module' => 'Puede ver modulo de permisos',
                'users' => 'Puede ver los usuarios',
                'user_edit' => 'Puede editar usuarios.',
                'user_banned' => 'Puede banear usuarios.',
                'user_permissions' => 'Puede ver los permisos',
                'user_permissions_add' => 'Puede actualizar los permisos',
                ]
            ],
            

    ];

    return $p;
}

function getUserYears(){
    $ya = date('Y');
    $ym = $ya - 18;
    $yo = $ym - 62;

    return [$ym,$yo];
 }

 function getMonths($mode, $key){
    $m = [
        '00' => 'Mes...',
        '01' => 'Enero',
        '02' => 'Febrero',
        '03' => 'Marzo',
        '04' => 'Abril',
        '05' => 'Mayo',
        '06' => 'Junio',
        '07' => 'Julio',
        '08' => 'Agosto',
        '09' => 'Septiembre',
        '10' => 'Octubre',
        '11' => 'Noviembre',
        '12' => 'Diciembre'
    ];
    if($mode == "list"){
        return $m;
    }else{
        return $m[$key];
    }
 }


?>
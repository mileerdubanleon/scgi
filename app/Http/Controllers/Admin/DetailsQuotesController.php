<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Quotes;
use App\Models\DetailsQuotes;

use Validator;

class DetailsQuotesController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view quotes
    public function getHome(){
        $details = DetailsQuotes::orderBy('id', 'desc')->paginate(25);
        $clients = Quotes::pluck('client_name', 'id'); 
        $data = ['details' => $details, 'clients' => $clients];
        return view('admin.detailsQuotes.home', $data);
    }

    // post add details quotes
    public function postDetailsQuotesAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'codigo' => 'required',
            'details_quotes' => 'required',
            'quantity' => 'required',
            'description' => 'required',
            'width_quotes' => 'required',
            'height_quotes' => 'required',
            'price_unit' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

            $details = new DetailsQuotes;
            $details->codigo = e($request->input('codigo'));
            $details->details_quotes = e($request->input('details_quotes'));
            $details->quantity = e($request->input('quantity'));
            $details->description = e($request->input('description'));
            $details->width_quotes = e($request->input('width_quotes'));
            $details->height_quotes = e($request->input('height_quotes'));
            $details->price_unit = e($request->input('price_unit'));
    
        if ($details->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }

    }
}

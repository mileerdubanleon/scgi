<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;

// models items
use App\Models\Items;
use Validator;
use Auth;


class ItemsController extends Controller
{
    //session
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view items
    public function getHome(){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'items')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver las cotizaciones.');
        }
        $items = items::orderBy('id', 'desc')->paginate(25);
        $data = ['items' => $items];
        return view('admin.items.home', $data);
    }
    

}

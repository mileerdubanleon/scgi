<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Client;
use Validator;
use Auth;

class ClientsController extends Controller
{
    //
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view clients
    public function getHome(Request $request){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'clients')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver la lista de clientes.');
        }

        $client = Client::orderBy('id', 'desc')->paginate(25);
        $email = $request->input('email');
        // Verifica si el correo ya existe en la base de datos
        $exists = Client::where('email', $email)->exists();
        $data = ['client' => $client, 'exists' => $exists];
        
        // Si la solicitud es AJAX, devuelve la respuesta JSON
        if ($request->ajax()) {
            return response()->json(['exists' => $exists]);
        }

        // Si es una solicitud normal, carga la vista
        return view('admin.clients.home', $data);
    }

    // post add clients
    public function postClientAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'client_name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
    
            $client = new Client;
            $client->client_name = e($request->input('client_name'));
            $client->address = e($request->input('address'));
            $client->email = e($request->input('email'));
            $client->phone = e($request->input('phone'));
    
        if ($client->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }
    }

    // search clients
    public function searchClients(Request $request){
        try {
            $term = $request->input('term');
            $clientes = Client::where('client_name', 'like', '%' . $term . '%')->get();

            return response()->json($clientes);
        } catch (\Exception $e) {
            \Log::error("Error: " . $e->getMessage());
            return response()->json(['error' => 'Internal Server Error'], 500);
        }
    }

    // details clients search select
    public function getClientDetails($id){
        $cliente = Client::find($id);

        return response()->json([
            'address' => $cliente->address,
            'email' => $cliente->email,
            'phone' => $cliente->phone,
        ]);
    }

    // get clients edit
    public function getClientEdit($id){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'client_edit')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para editar clientes.');
        }
        $client = Client::find($id);
        $data = ['client' => $client];
        return view('admin.clients.edit', $data);
    }

    // post clients edit
    public function postClientEdit(Request $request, $id){
        $rules = [
            'client_name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ];
        $messages = [
            'client_name.required' => 'Se requiere de un nombre para los clientes.',
            'address.required' => 'Se requiere de un codigo para los clientes.',
            'email.required' => 'Se requiere de un precio para los clientes.',
            'phone.required' => 'Se requiere de un precio de venta para los clientes.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger'); 
        else:
            $client = Client::find($id);
            $client->client_name = e($request->input('client_name'));
            $client->address = e($request->input('address'));
            $client->email = e($request->input('email'));
            $client->phone = e($request->input('phone'));
            
            if($client->save()):
                return back()->with('message', 'Actualizado con éxito')->with('typealert', 'success');
            endif;
        endif;
    }

    // clients delete
    public function getClientDelete($id) {
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'client_delete')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para eliminar clientes.');
        }
        $client = Client::find($id);
        if($client->delete()):
            return back()->with('message', 'Borrado con éxito.')->with('typealert', 'success');
        endif; 
    }

}

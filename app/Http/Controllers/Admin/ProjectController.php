<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Project;
use App\Models\Employee;
use Validator;
use Auth;

class ProjectController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view project
    public function getHome(){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'project')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver los empleados.');
        }
        $project = Project::orderBy('id', 'desc')->paginate(25);
        $namiven = Employee::pluck('name', 'id');
        $data = ['project' => $project, 'namiven' => $namiven];
        return view('admin.project.home', $data);
    }

    // post add projects
    public function postProjectAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'price_company' => 'required',
            'price_material' => 'required',
            'price_employee' => 'required'
        ]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
    
            $project = new Project;
            $project->name = e($request->input('name'));
            $project->description = e($request->input('description'));
            $project->price = e($request->input('price'));
            $project->price_company = e($request->input('price_company'));
            $project->price_material = e($request->input('price_material'));
            $project->employee_name = e($request->input('employee_name'));
            $project->price_employee = e($request->input('price_employee'));
            $project->state = 1;
    
        if ($project->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }
    }

    // get project edit
    public function getProjectEdit($id){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'project_edit')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para editar los proyectos.');
        }
        $project = Project::find($id);
        $data = ['project' => $project];
        return view('admin.project.edit', $data);
    }

    // post clients edit
    public function postProjectEdit(Request $request, $id){
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'price_company' => 'required',
            'price_material' => 'required',
            'price_employee' => 'required'
        ];
        $messages = [
            'name.required' => 'Se requiere un nombre para los proyectos.',
            'description.required' => 'Se requiere de una descripcion para los proyectos.',
            'price.required' => 'Se requiere de un precio para los proyectos.',
            'price_company.required' => 'Se requiere de un precio de compra para los proyectos.',
            'price_material.required' => 'Se requiere de un precio material para los proyectos.',
            'price_employee.required' => 'Se requiere de un presupuesto para los proyectos.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger'); 
        else:
            $project = Project::find($id);
            $project->name = e($request->input('name'));
            $project->description = e($request->input('description'));
            $project->price = e($request->input('price'));
            $project->price_company = e($request->input('price_company'));
            $project->price_material = e($request->input('price_material'));
            $project->price_employee = e($request->input('price_employee'));
            
            if($project->save()):
                return back()->with('message', 'Actualizado con éxito')->with('typealert', 'success');
            endif;
        endif;
    }

    // projects delete
    public function getProjectDelete($id) {
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'project_delete')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para eliminar los proyectos.');
        }
        $project = Project::find($id);
        if($project->delete()):
            return back()->with('message', 'Borrado con éxito.')->with('typealert', 'success');
        endif; 
    }

    // change btn disabled and enabled
    public function cambiarEstadoProject(Request $request){
        $itemId = $request->input('itemId');
        $currentState = $request->input('currentState');

        // Encuentra el elemento en la base de datos por su ID
        $inventoryItem = Project::find($itemId);

        if (!$inventoryItem) {
            return response()->json(['error' => 'Elemento no encontrado'], 404);
        }

        // Cambia el estado según la lógica deseada
        $nuevoEstado = $currentState === '1' ? '0' : '1';
        $inventoryItem->update(['state' => $nuevoEstado]);

        // Devuelve una respuesta JSON con el nuevo estado
        return response()->json(['newState' => $nuevoEstado]);
    }

}

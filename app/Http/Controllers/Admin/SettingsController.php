<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Settings;
use Auth;

class SettingsController extends Controller
{
    //autenticate
    public function __Construct(){
        $this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view home
    public function getHome(){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'settings')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver la seccion de configuración.');
        }
        $setting = Settings::orderBy('id', 'desc')->paginate(25);
        $data = ['setting' => $setting];
        return view('admin.settings.home', $data);
    }

    // add images
    // add text, description, information
    public function PostInformationSettings(Request $request){
        // Validar la solicitud
        $request->validate([
            'image_background' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_background_mobile' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title_background' => 'required',
            'description_background' => 'required',
        ]);
    
        // Obtener la instancia del modelo Settings o crear una nueva si no existe
        $setting = Settings::firstOrNew();
    
        // Actualizar la información de la configuración
        $setting->title_background = $request->input('title_background');
        $setting->description_background = $request->input('description_background');
    
        // Manejar la subida de la imagen de inicio
        if ($request->hasFile('image_background')) {
            // Eliminar la imagen anterior si existe
            if ($setting->image_background) {
                Storage::delete('public/' . $setting->image_background);
            }
    
            $imageName = 'background.jpg'; // Especifica el nombre del archivo
            $imagePath = $request->file('image_background')->storeAs('uploads_settings', $imageName, 'public');
            $setting->image_background = 'uploads_settings/' . $imageName;
        }
    
        // Manejar la subida de la imagen de inicio móvil
        if ($request->hasFile('image_background_mobile')) {
            // Eliminar la imagen anterior si existe
            if ($setting->image_background_mobile) {
                Storage::delete('public/' . $setting->image_background_mobile);
            }
    
            $mobileImageName = 'background_mobile.jpg'; // Especifica el nombre del archivo
            $mobileImagePath = $request->file('image_background_mobile')->storeAs('uploads_settings', $mobileImageName, 'public');
            $setting->image_background_movile = 'uploads_settings/' . $mobileImageName;
        }
    
        // Guardar los cambios en la base de datos solo si hay un archivo para cargar
        if ($request->hasFile('image_background') || $request->hasFile('image_background_mobile')) {
            $setting->save();
        }
    
        // Redireccionar o mostrar un mensaje de éxito
        return redirect()->route('settings')->with('success', 'Configuración actualizada correctamente.');
    }

    // view section publiicty
    public function getPublicityHome(){
        return view('admin.settings.publicity.home');
    }

}
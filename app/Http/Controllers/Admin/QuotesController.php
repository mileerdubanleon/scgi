<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quotes;

use Validator;

class QuotesController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view quotes
    public function getHome(){
        $quotes = Quotes::with('details')->orderBy('id', 'desc')->paginate(25);
        $data = ['quotes' => $quotes];
        return view('admin.quotes.home', $data);
    }

    // post add clients
    public function postQuotesAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'client_name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
    
            $quotes = new Quotes;
            $quotes->client_name = e($request->input('client_name'));
            $quotes->address = e($request->input('address'));
            $quotes->email = e($request->input('email'));
            $quotes->phone = e($request->input('phone'));
    
        if ($quotes->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Employee;
use Validator;
use Auth;

class EmployeeController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view employee
    public function getHome(Request $request){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'employee')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver los empleados.');
        }
        $employee = Employee::orderBy('id', 'desc')->paginate(25);
        $identification = $request->input('identification');
        // Verifica si el correo ya existe en la base de datos
        $exists = Employee::where('identification', $identification)->exists();
        $data = ['employee' => $employee, 'exists' => $exists];
        
        // Si la solicitud es AJAX, devuelve la respuesta JSON
        if ($request->ajax()) {
            return response()->json(['exists' => $exists]);
        }
        
        return view('admin.employee.home', $data);
    }

    // post add employee
    public function postEmployeeAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'lastname' => 'required',
            'type_document' => 'required',
            'identification' => 'required',
            'date' => 'required',
            'salary' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
            $employee = new Employee;
            $employee->name = e($request->input('name'));
            $employee->lastname = e($request->input('lastname'));
            $employee->type_document = e($request->input('type_document'));
            $employee->identification = e($request->input('identification'));
            $employee->date = e($request->input('date'));
            $employee->salary = e($request->input('salary'));
            $employee->state = 1;
    
        if ($employee->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }

    }

    // get employee edit
    public function getEmployeeEdit($id){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'employee_edit')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para editar los empleados.');
        }
        $employee = Employee::find($id);
        $data = ['employee' => $employee];
        return view('admin.employee.edit', $data);
    }

    // post employee edit
    public function postEmployeeEdit(Request $request, $id){
        $rules = [
            'name' => 'required',
            'lastname' => 'required',
            'identification' => 'required',
            'date' => 'required',
            'salary' => 'required',
        ];
        $messages = [
            'name.required' => 'Se requiere de un nombre para el empleado.',
            'lastname.required' => 'Se requiere un apellido para el empleado.',
            'identification.required' => 'Se requiere de una identificación para el empleado.',
            'date.required' => 'Se requiere de una fecha de nacimiento.',
            'salary.required' => 'Se requiere de un salario.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger'); 
        else:
            $employee = Employee::find($id);
            $employee->name = e($request->input('name'));
            $employee->lastname = e($request->input('lastname'));
            $employee->identification = e($request->input('identification'));
            $employee->date = e($request->input('date'));
            $employee->salary = e($request->input('salary'));
            
            if($employee->save()):
                return back()->with('message', 'Actualizado con éxito')->with('typealert', 'success');
            endif;
        endif;
    }

    // employee delete
    public function getEmployeeDelete($id) {
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'employee')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para eliminar los empleados.');
        }
        $employee = Employee::find($id);
        if($employee->delete()):
            return back()->with('message', 'Borrado con éxito.')->with('typealert', 'success');
        endif; 
    }

    // change btn disabled and enabled
    public function cambiarEstadoEmployee(Request $request){
        $itemId = $request->input('itemId');
        $currentState = $request->input('currentState');

        // Encuentra el elemento en la base de datos por su ID
        $employeeItem = Employee::find($itemId);

        if (!$employeeItem) {
            return response()->json(['error' => 'Elemento no encontrado'], 404);
        }

        // Cambia el estado según la lógica deseada
        $nuevoEstado = $currentState === '1' ? '0' : '1';
        $employeeItem->update(['state' => $nuevoEstado]);

        // Devuelve una respuesta JSON con el nuevo estado
        return response()->json(['newState' => $nuevoEstado]);
    }


}

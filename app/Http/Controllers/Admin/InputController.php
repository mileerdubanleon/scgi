<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Input;
use App\Models\Inventory;

use Validator;
use Auth;

class InputController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view input
    public function getHome(){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'input')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver las entradas.');
        }
        $input = Input::orderBy('id', 'desc')->paginate(25);
        $namiven = Inventory::pluck('name', 'id');
        $data = ['input' => $input, 'namiven' => $namiven];
        return view('admin.input.home', $data);
    }

    // post add input
    public function postInputAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

            $input = new Input;
            $input->name_inventory = e($request->input('name_inventory'));
            $input->quantity = e($request->input('quantity'));
    
        if ($input->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }
    }

    // get input delete
    public function getInputDelete($id){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'input_delete')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para eliminar las entradas.');
        }
        $input = Input::find($id);
        if($input->delete()):
            return back()->with('message', 'Borrado con éxito.')->with('typealert', 'success');
        endif; 
    } 

}

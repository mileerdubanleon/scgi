<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Output;
use App\Models\Inventory;
use App\Models\Employee;

use Validator;
use Auth;

class OutputController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view output
    public function getHome(){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'output')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para ver las salidas.');
        }
        $output = Output::orderBy('id', 'desc')->paginate(25);
        $namiven = Inventory::pluck('name', 'id');
        $ouemp = Employee::pluck('name', 'id');
        $data = ['output' => $output, 'namiven' => $namiven,  'ouemp' => $ouemp];
        return view('admin.output.home', $data);
    }

    // post add output
    public function postOutputAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

            $output = new Output;
            $output->name_inventory_output = e($request->input('name_inventory_output'));
            $output->output_employee = e($request->input('output_employee'));
            $output->quantity = e($request->input('quantity'));
    
        if ($output->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }

    }

    // get ouput delete
    public function getOutputDelete($id){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'output_delete')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para eliminar las salidas.');
        }
        $output = Output::find($id);
        if($output->delete()):
            return back()->with('message', 'Borrado con éxito.')->with('typealert', 'success');
        endif; 
    } 

}

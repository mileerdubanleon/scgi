<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Quotation;
use App\Models\Inventory;
use App\Models\Employee;
use App\Models\Project;
use App\Models\User;

class DashboardController extends Controller
{
    public function __Construct(){
        $this->middleware('auth');
        $this->middleware('isadmin');
    }
    
    // view dashboard
    public function getDashboard(){
    	// Obtener el número actual de clientes
        $numClientes = Client::count();
        // Obtener el número actual de cotizaciones
        $numQuotation = Quotation::count();
        // Obtener el número actual de product
        $numInventory = Inventory::count();
        // Obtener el número actual de empleados
        $numEmployee = Employee::count();
        // Obtener el número actual de proyectos
        $numProject = Project::count();
        // Obtener el número actual de usuarios
        $numUsers = User::count();


        return view('admin.dashboard', compact('numClientes', 'numQuotation', 'numInventory', 'numEmployee','numProject','numUsers'));
    }


}

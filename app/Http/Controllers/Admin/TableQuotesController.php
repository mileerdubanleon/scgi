<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//models
use App\Models\DetailsQuotes;
use App\Models\Quotes;
//pdf
use PDF;



class TableQuotesController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view table quotes
    public function getHome(){
        $table = Quotes::orderBy('id', 'desc')->paginate(25);
        $data = ['table' => $table];
        return view('admin.detailsQuotes.table', $data);
    }

    public function getDetailsCli($id) {
        // Buscar el registro de Quotes por su ID
        $quote = Quotes::findOrFail($id);
    
        // Obtener los registros de TableQuotes relacionados con el Quotes
        $tableQuotes = $quote->tableQuotes;
    
        $data = [
            'quote' => $quote,
            'tableQuotes' => $tableQuotes,
        ];
    
        return view('admin.detailsQuotes.tableQuotes', $data);
    }

    public function getPdf($codigo) {
        // Buscar el registro de DetailsQuotes por su código
        $quote = DetailsQuotes::where('codigo', $codigo)->with('quote')->first();
    
        if (!$quote) {
            abort(404); // Otra acción en caso de que el registro no se encuentre
        }
    
        // Obtener los registros de DetailsQuotes relacionados con el TableQuotes
        $detailsQuotes = $quote->detailsQuotes;
    
        $data = [
            'quote' => $quote,
            'detailsQuotes' => $detailsQuotes,
        ];
    
        // Cargar la vista y crear el PDF
        $pdf = PDF::loadView('admin.detailsQuotes.pdf', $data);
    
        // Mostrar o descargar el PDF
        return $pdf->stream('nombre_del_archivo.pdf');
        // return $pdf->download($codigo . '.pdf'); Se descarga con el código
    }

}

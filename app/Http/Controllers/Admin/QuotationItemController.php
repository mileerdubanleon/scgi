<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\Quotation;
use App\Models\DetailsQuotation;
use App\Models\Items;
use App\Models\Client;

class QuotationItemController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }
    
    public function guardarCotizacion(Request $request){
        // Validación
        $validator = Validator::make($request->all(), [
            'cliente' => 'required',
            'total_general' => 'required',
            'items.*.quantity' => 'required|numeric',
            'items.*.description' => 'required|string',
            'items.*.width_quotes' => 'required|numeric',
            'items.*.height_quotes' => 'required|numeric',
            'items.*.price_unit' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        try {
            // Iniciar una transacción
            \DB::beginTransaction();

            // Obtener el ID del cliente desde la solicitud
            $clienteId = $request->input('cliente');
            
            // Crear una nueva cotización
            $cotizacion = new Quotation();
            $cotizacion->price_total = $request->input('total_general');
            $cotizacion->id_cliente = $clienteId;
            $cotizacion->save();

            // Obtener el ID asignado de la cotización
            $idCotizacion = $cotizacion->getKey();

            // Iterar sobre los ítems y guardar en la tabla 'items' y 'details_quotation'
            foreach (json_decode($request->input('items'), true) as $itemData) {
                $nuevoItem = new Items();
                $nuevoItem->quantity = $itemData['quantity'];
                $nuevoItem->description = $itemData['description'];
                $nuevoItem->width_quotes = $itemData['width_quotes'];
                $nuevoItem->height_quotes = $itemData['height_quotes'];
                $nuevoItem->price_unit = $itemData['price_unit'];
                $nuevoItem->save();

                // Relacionar el ítem con la cotización en la tabla 'details_quotation'
                $detalleCotizacion = new DetailsQuotation();
                $detalleCotizacion->id_cotizacion = $idCotizacion;
                $detalleCotizacion->id_items = $nuevoItem->id;
                $detalleCotizacion->save();
            }

            // Commit a la transacción
            \DB::commit();

            return response()->json(['success' => 'Guardado con éxito.']);
        } catch (\Exception $e) {
            // Rollback de la transacción en caso de error
            \DB::rollback();

            return response()->json(['errors' => ['general' => 'Error al guardar los datos']]);
        }
    }

    // view consulta de cotizaciones 
    public function quotationsHome(){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'quotationsConsult')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para consultar las cotizaciones.');
        }
        return view('admin.items.consulta');
    }

    // Método para buscar cotizaciones por nombre de cliente
    public function consultaQuotations(Request $request){
        // Validación
        $validator = Validator::make($request->all(), [
            'nombre_cliente' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        try {
            // Obtener el cliente por nombre
            $cliente = Client::where('client_name', $request->input('nombre_cliente'))->first();

            if (!$cliente) {
                return response()->json(['errors' => ['nombre_cliente' => 'Cliente no encontrado']]);
            }

            // Obtener las cotizaciones del cliente con detalles de items
            $cotizaciones = Quotation::where('id_cliente', $cliente->id)
                ->with(['details_quotations.item']) // Cargar los detalles y los items asociados
                ->get();

            // Retornar la estructura JSON esperada
            return response()->json([
                'clientDetails' => [
                    'address' => $cliente->address,
                    'email' => $cliente->email,
                    'phone' => $cliente->phone,
                ],
                'cotizaciones' => $cotizaciones,
            ]);
        } catch (\Exception $e) {
            return response()->json(['errors' => ['general' => 'Error al buscar cotizaciones: ' . $e->getMessage()]]);
        }
    }
    
    public function getPdf($id) {
        // Verificar si existe el registro de DetailsQuotation por su código
        $quote = DetailsQuotation::where('id_cotizacion', $id)->with('quotation.client', 'item')->first();
    
        if (!$quote) {
            abort(404); // Otra acción en caso de que el registro no se encuentre
        }
    
        // Calcular subtotal, IVA y total
        $subtotal = 0;
    
        foreach ($quote->quotation->detailsQuotation as $detailsQuote) {
            $subtotal += $detailsQuote->quantity * $detailsQuote->item->price_unit;
        }
    
        // Calcular IVA (7%)
        $iva = $subtotal * 0.07;
    
        // Calcular el Total
        $total = $subtotal + $iva;
    
        $data = [
            'quote' => $quote,
            'subtotal' => $subtotal,
            'iva' => $iva,
            'total' => $total,
        ];
    
        // Cargar la vista y crear el PDF
        $pdf = \PDF::loadView('admin.items.pdf', $data);
    
        // Mostrar o descargar el PDF
        return $pdf->stream('nombre_del_archivo.pdf');
        // return $pdf->download($codigo . '.pdf'); Se descarga con el código
    }
}
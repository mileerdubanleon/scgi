<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Inventory;
use App\Models\Input;
use App\Models\Output;
use Validator;
use Auth;

class InventoryController extends Controller
{
    public function __Construct(){
    	$this->middleware('auth');
        $this->middleware('isadmin');
    }

    // view inventory
    public function getHome(Request $request){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'inventario')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para abrir el inventario.');
        }
        $inventory = Inventory::orderBy('id', 'desc')->paginate(25);
        $codigo = $request->input('codigo');
        // Verifica si el codigo ya existe en la base de datos
        $exists = Inventory::where('codigo', $codigo)->exists();
        $data = ['inventory' => $inventory, 'exists' => $exists];

        
        // Si la solicitud es AJAX, devuelve la respuesta JSON
        if ($request->ajax()) {
            return response()->json(['exists' => $exists]);
        }
        return view('admin.inventory.home', $data);
    }

    // view statistics
    public function getStatistics(){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'stadisticas')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para abrir las estadísticas.');
        }
        $inventory = Inventory::with('output')->orderBy('id', 'desc')->paginate(25);

        // Recorre los resultados y calcula la cantidad disponible
        $inventory->transform(function ($item, $key) {
            $item->available_quantity = $item->inputs()->sum('quantity') - $item->outputs()->sum('quantity');
            return $item;
        });

        // Calcula la suma de los precios unitarios
        $totalPrecioUnitario = $inventory->sum(function($item) {
            return $item->price * $item->available_quantity;
        });

        // Calcula la suma de los precios de venta
        $totalPrecioVenta = $inventory->sum(function($item) {
            return $item->price_sale * $item->available_quantity;
        });

        $data = ['inventory' => $inventory, 'totalPrecioUnitario' => $totalPrecioUnitario, 'totalPrecioVenta' => $totalPrecioVenta];
        return view('admin.inventory.statistics', $data);
    }

    // post add inventory
    public function postInventoryAdd(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'codigo' => 'required',
            'price' => 'required',
            'price_sale' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

            $inventory = new Inventory;
            $inventory->name = e($request->input('name'));
            $inventory->codigo = e($request->input('codigo'));
            $inventory->price = e($request->input('price'));
            $inventory->price_sale = e($request->input('price_sale'));
            $inventory->state = 1;
    
        if ($inventory->save()) {
            return response()->json(['success' => 'Guardado con éxito.']);
        }
    }

    // get inventory edit
    public function getInventoryEdit($id){
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'inventario_edit')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para editar un producto.');
        }
        $inventory = Inventory::find($id);
        $data = ['inventory' => $inventory];
        return view('admin.inventory.edit', $data);
    }

    // post inventory edit
    public function postInventoryEdit(Request $request, $id){
        $rules = [
            'name' => 'required',
            'codigo' => 'required',
            'price' => 'required',
            'price_sale' => 'required',
        ];
        $messages = [
            'name.required' => 'Se requiere de un nombre para el inventario.',
            'codigo.required' => 'Se requiere de un codigo para el inventario.',
            'price.required' => 'Se requiere de un precio para el inventario.',
            'price_sale.required' => 'Se requiere de un precio de venta para el inventario.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger'); 
        else:
            $inventory = Inventory::find($id);
            $inventory->name = e($request->input('name'));
            $inventory->codigo = e($request->input('codigo'));
            $inventory->price = e($request->input('price'));
            $inventory->price_sale = e($request->input('price_sale'));
            
            if($inventory->save()):
                return back()->with('message', 'Actualizado con éxito')->with('typealert', 'success');
            endif;
        endif;
    }

    // inventory delete
    public function getInventoryDelete($id) {
        // Verificar si el usuario tiene permisos
        if (!kvfj(Auth::user()->permissions, 'inventario_delete')) {
            // Handle unauthorized access (redirect, show error message, etc.)
            abort(403, 'No tienes permisos para editar un producto.');
        }
        $inventory = Inventory::find($id);
        if($inventory->delete()):
            return back()->with('message', 'Borrado con éxito.')->with('typealert', 'success');
        endif; 
    }

    // change btn disabled and enabled
    public function cambiarEstado(Request $request){
        $itemId = $request->input('itemId');
        $currentState = $request->input('currentState');

        // Encuentra el elemento en la base de datos por su ID
        $inventoryItem = Inventory::find($itemId);

        if (!$inventoryItem) {
            return response()->json(['error' => 'Elemento no encontrado'], 404);
        }

        // Cambia el estado según la lógica deseada
        $nuevoEstado = $currentState === '1' ? '0' : '1';
        $inventoryItem->update(['state' => $nuevoEstado]);

        // Devuelve una respuesta JSON con el nuevo estado
        return response()->json(['newState' => $nuevoEstado]);
    }

}

<?php

namespace App\Http\Controllers;

use Validator, Image, Auth, Config, Str, Hash;

use App\Models\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(){
		$this->middleware('auth');
    }
    // view edit profile
	public function getAccountEdit(){
        $birthday = (is_null(Auth::user()->birthday)) ? [null,null,null] : explode('-', Auth::user()->birthday);
        $data = ['birthday' => $birthday];
		return view('user.account_edit', $data);
	}
    // update profile
    public function updateAvatar(Request $request){
        $validator = Validator::make($request->all(), [
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ], [
            'avatar.max' => 'La imagen no debe superar los 2 MB',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->with('message', 'Se ha producido un error')
                ->with('typealert', 'danger')
                ->withInput();
        }

        $path = '/uploads_user/' . Auth::id();
        $upload_path = public_path($path);

        // Obtener el nombre del avatar actual antes de actualizarlo
        $oldAvatar = Auth::user()->avatar;
        $filename = 'av_' . Auth::id() . '_' . time() . '.' . $request->file('avatar')->getClientOriginalExtension();

        // Guardar la imagen original en la carpeta public/uploads_user
        $request->file('avatar')->move($upload_path, $filename);

        // Lógica para actualizar la base de datos con el nuevo nombre de archivo
        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();

        // Construir la URL completa de la imagen
        $avatar_url = asset($path . '/' . $filename);

        // Eliminar el avatar anterior
        if ($oldAvatar) {
            unlink($upload_path . '/' . $oldAvatar);
        }

        return redirect()->route('account_edit')
            ->with('avatar_url', $avatar_url)
            ->with('message', 'Avatar actualizado con éxito.')
            ->with('typealert', 'success');
    }
    // changed password profile
    public function postAccountPassword(Request $request){
        $rules = [
            'apassword' => 'required|min:8',
            'password' => 'required|min:8' ,
            'cpassword' => 'required|min:8|same:password'
        ];

        $messages = [
            'apassword.required' => 'Escriba su contraseña actual',
            'apassword.min' => 'La contraseña actual debe de tener al menos 8 caracteres',
            'password.required' => 'Escriba su nueva contraseña',
            'password.min' => 'Su nueva contraseña debe de tener al menos 8 caracteres',
            'cpassword.required' => 'Confirme su nueva contraseña',
            'cpassword.min' => 'La confirmación de la contraseña debe de tener al menos 8 caracteres',
            'cpassword.same' => 'Las contraseñas no coinciden'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger')->withInput(); 
        else:
            $u = User::find(Auth::id());
            if(Hash::check($request->input('apassword'), $u->password)):
                $u->password = Hash::make($request->input('password'));
                if($u->save()):
                    return back()->with('message','La contraseña se actualizo con éxito')->with('typealert','success');
                endif;
            else:
                return back()->with('message','Su contraseña actual es errónea')->with('typealert','danger'); 
            endif;    
        endif;
    }
    // 
    public function postAccountInfo(Request $request){
        $rules = [
            'name' => 'required',
            'lastname' => 'required' ,
            'phone' => 'required|min:10',
            'year' => 'required',
            'day' => 'required'
        ];

        $messages = [
            'name.required' => 'Su nombre es requerido',
            'lastname.min' => 'Su apellido es requerido',
            'phone.required' => 'Su numero de teléfono es requerido',
            'phone.min' => 'El numero de teléfono debe de tener como minimo 10 dígitos',
            'year.required' => 'Su año de nacimiento es requerido',
            'day.required' => 'Su día de nacimiento es requerido'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()):
            return back()->withErrors($validator)->with('message','Se ha producido un error')->with('typealert','danger')->withInput(); 
        else:
            $date = $request->input('year').'-'.$request->input('month').'-'.$request->input('day');
            $u = User::find(Auth::id());
            $u->name = e($request->input('name'));
            $u->lastname = e($request->input('lastname'));
            $u->phone = e($request->input('phone'));
            $u->birthday = date("Y-m-d", strtotime($date));
            $u->gender = e($request->input('gender'));
            if($u->save()):
                return back()->with('message', 'Su información se actualizo con éxto')->with('typealert','success');
            endif;
        endif;
    }

}

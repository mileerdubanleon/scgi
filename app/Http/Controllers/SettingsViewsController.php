<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Settings;

class SettingsViewsController extends Controller
{
    //
    public function mostrarPaginaInicio(){
        $setting = Settings::first();
        if (!$setting) {
            $setting = new Settings(); // Crear una instancia vacía si no hay configuración existente
        }
        return view('welcome', compact('setting'));
    }
}

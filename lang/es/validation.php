<?php
return [
    // ...

    'custom' => [
        // settings
        'image_background' => [
            'required' => 'El campo imagen de inicio es obligatorio.',
        ],
        'image_background_mobile' => [
            'required' => 'El campo imagen de inicio movíl es obligatorio.',
        ],
        'title_background' => [
            'required' => 'El campo titulo de inicio es obligatorio.',
        ],
        'description_background' => [
            'required' => 'El campo imagen de inicio es obligatorio.',
        ],
        // clients
        'client_name' => [
            'required' => 'El campo nombre es obligatorio.',
        ],
        'address' => [
            'required' => 'El campo dirección es obligatorio.',
        ],
        'email' => [
            'required' => 'El campo email es obligatorio.',
        ],
        'phone' => [
            'required' => 'El campo teléfono es obligatorio.',
        ],
        // end clients
        'avatar' => [
            'required' => 'El campo avatar es obligatorio.',
        ],
        'name' => [
            'required' => 'El campo nombre es obligatorio.',
        ],
        'description' => [
            'required' => 'El campo descripción es obligatorio.',
        ],
        'price' => [
            'required' => 'El campo precio es obligatorio.',
        ],
        'price_company' => [
            'required' => 'El campo presupuesto de la compañía es obligatorio.',
        ],
        'price_material' => [
            'required' => 'El campo precio de material es obligatorio.',
        ],
        'employee_name' => [
            'required' => 'El campo empleado encargado es obligatorio.',
        ],
        'price_employee' => [
            'required' => 'El campo presupuesto de empleado es obligatorio.',
        ],
        'price_sale' => [
            'required' => 'El campo precio de venta es obligatorio.',
        ],
        'lastname' => [
            'required' => 'El campo apellidos es obligatorio.',
        ],
        'identification' => [
            'required' => 'Ingrese un numero de documento.',
        ],
        'date' => [
            'required' => 'Ingrese el cumpleaños',
        ],
        'salary' => [
            'required' => 'Ingrese un salario',
        ],
        'quantity' => [
            'required' => 'Ingrese una cantidad',
        ],
        'codigo' => [
            'required' => 'Ingrese una codigo',
        ],
    ],

    // ...
];
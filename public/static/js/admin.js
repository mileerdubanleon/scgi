// routes active
document.addEventListener('DOMContentLoaded', function() {
    let route = document.querySelector('meta[name="routeName"]').getAttribute('content');
    let menuLink = document.querySelector('.lk-' + route);
    
    if (menuLink) {
        menuLink.classList.add('active');
    }
});

/* Preloader */
$(window).on('load', function() {
    var preloaderFadeOutTime = 500;
    function hidePreloader() {
        var preloader = $('.spinner-wrapper');
        setTimeout(function() {
            preloader.fadeOut(preloaderFadeOutTime);
        }, 200);
    }
    hidePreloader();
});
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\SettingsViewsController@mostrarPaginaInicio')->name('pagina_inicio');

// Route Auth
Route::get('/login', 'App\Http\Controllers\ConnectController@getLogin');
Route::post('/login', 'App\Http\Controllers\ConnectController@postLogin')->name('login');
Route::get('/register', 'App\Http\Controllers\ConnectController@getRegister')->name('register');
Route::post('/register', 'App\Http\Controllers\ConnectController@postRegister')->name('register_post');
Route::get('/logout', 'App\Http\Controllers\ConnectController@getLogout')->name('logout');

// Module Users Edit
Route::get('/account/edit', 'App\Http\Controllers\UserController@getAccountEdit')->name('account_edit'); //view edit profile user public
Route::post('/account/edit/avatar', 'App\Http\Controllers\UserController@updateAvatar')->name('account_avatar_edit');
Route::post('/account/edit/password', 'App\Http\Controllers\UserController@postAccountPassword')->name('account_password_edit');
Route::post('/account/edit/info', 'App\Http\Controllers\UserController@postAccountInfo')->name('account_info_edit');

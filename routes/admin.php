<?php 

    Route::prefix('/admin')->group(function(){
	    Route::get('/', 'App\Http\Controllers\Admin\DashboardController@getDashboard')->name('dashboard');

        //Gestión Clientes
        Route::get('/clientes', 'App\Http\Controllers\Admin\ClientsController@getHome')->name('clients');//GET
        Route::post('/clients/add', 'App\Http\Controllers\Admin\ClientsController@postClientAdd')->name('client_add');//POST
        // search clients quotes
        Route::get('/search-client', 'App\Http\Controllers\Admin\ClientsController@searchClients')->name('search-client');//GET
        Route::get('/get-client-details/{clienteId}', 'App\Http\Controllers\Admin\ClientsController@getClientDetails')->name('get-client-details');//GET
        // end search clients quotes
        
        Route::get('/clients/{id}/edit', 'App\Http\Controllers\Admin\ClientsController@getClientEdit')->name('client_edit');//GET
        Route::post('/clients/{id}/edit', 'App\Http\Controllers\Admin\ClientsController@postClientEdit')->name('client_edit_post');//POST
        Route::get('/clients/{id}/delete', 'App\Http\Controllers\Admin\ClientsController@getClientDelete')->name('client_delete');//GET delete

        //Gestión cotizaciones/items ajax
        Route::get('/items', 'App\Http\Controllers\Admin\ItemsController@getHome')->name('items');//GET
        Route::post('/guardarCotizacionItem', 'App\Http\Controllers\Admin\QuotationItemController@guardarCotizacion')->name('guardarCotizacionItem');//POST
        Route::get('/quotationsConsult', 'App\Http\Controllers\Admin\QuotationItemController@quotationsHome')->name('quotationsConsult');//GET
        Route::post('/consultaQuotations', 'App\Http\Controllers\Admin\QuotationItemController@consultaQuotations')->name('consultaQuotations');//POST
        Route::get('/quotationPDF/{id}/pdf', 'App\Http\Controllers\Admin\QuotationItemController@getPdf')->name('quotationPDF');//GET

        //Gestion inventario
        Route::get('/inventario', 'App\Http\Controllers\Admin\InventoryController@getHome')->name('inventario');//GET
        Route::post('/inventario/add', 'App\Http\Controllers\Admin\InventoryController@postInventoryAdd')->name('inventario_add');//POST
        Route::get('/inventario/{id}/edit', 'App\Http\Controllers\Admin\InventoryController@getInventoryEdit')->name('inventario_edit');//GET
        Route::post('/inventario/{id}/edit', 'App\Http\Controllers\Admin\InventoryController@postInventoryEdit')->name('inventario_edit_post');//POST
        Route::get('/inventario/{id}/delete', 'App\Http\Controllers\Admin\InventoryController@getInventoryDelete')->name('inventario_delete');//GET delete
        //Gestion estadisticas
        Route::get('/estadisticas', 'App\Http\Controllers\Admin\InventoryController@getStatistics')->name('stadisticas');//GET
        //Gestion btn state
        Route::post('/cambiarEstado', 'App\Http\Controllers\Admin\InventoryController@cambiarEstado')->name('cambiarEstado');//post

        //Gestion entradas
        Route::get('/entradas', 'App\Http\Controllers\Admin\InputController@getHome')->name('entradas');//GET view
        Route::post('/input/add', 'App\Http\Controllers\Admin\InputController@postInputAdd')->name('input_add');//POST add
        Route::get('/input/{id}/delete', 'App\Http\Controllers\Admin\InputController@getInputDelete')->name('input_delete');//GET delete
        
        //Gestion salidas
        Route::get('/salidas', 'App\Http\Controllers\Admin\OutputController@getHome')->name('salidas');//GET
        Route::post('/output/add', 'App\Http\Controllers\Admin\OutputController@postOutputAdd')->name('output_add');//POST
        Route::get('/output/{id}/delete', 'App\Http\Controllers\Admin\OutputController@getOutputDelete')->name('output_delete');//GET delete

        //Gestion empleados
        Route::get('/empleados', 'App\Http\Controllers\Admin\EmployeeController@getHome')->name('employee');//GET
        Route::post('/empleados/add', 'App\Http\Controllers\Admin\EmployeeController@postEmployeeAdd')->name('employee_add');//POST
        Route::get('/empleados/{id}/edit', 'App\Http\Controllers\Admin\EmployeeController@getEmployeeEdit')->name('employee_edit');//GET
        Route::post('/empleados/{id}/edit', 'App\Http\Controllers\Admin\EmployeeController@postEmployeeEdit')->name('employee_edit_post');//POST
        Route::get('/empleados/{id}/delete', 'App\Http\Controllers\Admin\EmployeeController@getEmployeeDelete')->name('employee_delete');//GET delete
        //Gestion btn state
        Route::post('/cambiarEstadoEmployee', 'App\Http\Controllers\Admin\EmployeeController@cambiarEstadoEmployee')->name('cambiarEstadoEmployee');//post

        //Gestion proyectos
        Route::get('/proyectos', 'App\Http\Controllers\Admin\ProjectController@getHome')->name('project');//GET
        Route::post('/proyectos/add', 'App\Http\Controllers\Admin\ProjectController@postProjectAdd')->name('project_add');//POST
        Route::get('/proyectos/{id}/edit', 'App\Http\Controllers\Admin\ProjectController@getProjectEdit')->name('project_edit');//GET
        Route::post('/proyectos/{id}/edit', 'App\Http\Controllers\Admin\ProjectController@postProjectEdit')->name('project_edit_post');//POST
        Route::get('/proyectos/{id}/delete', 'App\Http\Controllers\Admin\ProjectController@getProjectDelete')->name('project_delete');//GET delete
        //Gestion btn state
        Route::post('/cambiarEstadoProject', 'App\Http\Controllers\Admin\ProjectController@cambiarEstadoProject')->name('cambiarEstadoProject');//post

        //Gestion design page principal
        Route::get('/settings', 'App\Http\Controllers\Admin\SettingsController@getHome')->name('settings');//GET
        Route::get('/publicity', 'App\Http\Controllers\Admin\SettingsController@getPublicityHome')->name('publicity');//GET
        // images
        Route::post('/settings/add', 'App\Http\Controllers\Admin\SettingsController@PostInformationSettings')->name('settings_information');//POST
        
        //Gestión usuarios
        Route::get('/usuarios', 'App\Http\Controllers\Admin\UsersController@getHome')->name('users');//GET
        Route::get('/users/{id}/edit', 'App\Http\Controllers\Admin\UsersController@getUserEdit')->name('user_edit');
        Route::post('/usuario/{id}/edit', 'App\Http\Controllers\Admin\UsersController@postUserEdit')->name('user_edit_post');
        Route::get('/user/{id}/banned', 'App\Http\Controllers\Admin\UsersController@getUserBanned')->name('user_banned');
        Route::get('/usuarios/{id}/permissions', 'App\Http\Controllers\Admin\UsersController@getUserPermissions')->name('user_permissions');//GET
        Route::post('/usuarios/{id}/permissions', 'App\Http\Controllers\Admin\UsersController@postUserPermissions')->name('user_permissions_add');//POST

    });

?>
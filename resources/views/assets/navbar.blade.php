<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
    <!-- Text Logo - Use this if you don't have a graphic logo -->
    <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Evolo</a> -->

    <!-- Image Logo -->
    <a class="navbar-brand logo-image " href="{{ url('/') }}"><img src="{{ asset('../static/images/logo.svg') }}" alt="alternative"></a>
    
    <!-- Mobile Menu Toggle Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-awesome fas fa-bars"></span>
        <span class="navbar-toggler-awesome fas fa-times"></span>
    </button>
    <!-- end of mobile menu toggle button -->

    <div class="collapse navbar-collapse px-2" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link page-scroll btn-shadow lead" href="#header">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link page-scroll btn-shadow lead" href="#services">Servicios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link page-scroll btn-shadow lead" href="#about">Nosotros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link page-scroll btn-shadow lead" href="#store">Tienda</a>
            </li>
            <li class="nav-item">
                <a class="nav-link page-scroll btn-shadow lead" href="#contact">Contáctenos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link page-scroll btn-shadow lead" href="#address">Ubicación</a>
            </li>

            @if (Auth::check())
                <!-- Dropdown Menu -->          
                <li class="nav-item dropdown px-2 btn-shadow">
                    <div class="nav-link dropdown-toggle page-scroll" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="text-dark lead">{{ Auth::user()->name }}</span>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('/account/edit') }}"><span class="item-text lead">Editar perfil</span></a>
                        <div class="dropdown-items-divide-hr"></div>
                            @if (Auth::user()->role == 1 || Auth::user()->role == 2)
                                <a class="dropdown-item my-3" href="{{ url('/admin') }}"><span class="item-text">Administración</span></a>
                            @endif
                        <a class="dropdown-item" href="{{ url('/logout') }}"><span class="item-text">Cerrar sesión</span></a>
                    </div>
                </li>
                <!-- end of dropdown menu -->
            @else
                <!-- Si el usuario no está autenticado, muestra los enlaces de inicio de sesión y registro -->
                <div class="border-right bg-dark"></div>
                <li class="nav-item">
                    <a class="nav-link page-scroll btn-shadow" href="{{ url('/login') }}"><span class="lead">Iniciar sesión</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll btn-shadow lead" href="{{ url('/register') }}">Crea una cuenta</a>
                </li>
            @endif
    </div>
    @if (Auth::check())
        {{-- img profile --}}
        @if(Auth::user()->avatar)
            <img src="{{ asset('/uploads_user/' . Auth::id() . '/' . Auth::user()->avatar) }}" class="shadow mx-3" style="width: 70px; height: 70px; border-radius: 50px; border: 2px solid #27aef7;" alt="Avatar">
        @else
            <img src="{{ asset('/static/images/default-avatar.png') }}" style="width: 70px; height: 70px; border-radius: 50px;" alt="Default Avatar">
        @endif
    @endif

    <span class="nav-item social-icons gap-3">
        <span class="fa-stack">
            <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x facebook "></i>
                <i class="fab fa-facebook-f fa-stack-2x text-dark"></i>
            </a>
        </span>
        <span class="fa-stack">
            <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x instagram"></i>
                <i class="fab fa-instagram fa-stack-2x text-dark"></i>
            </a>
        </span>
    </span>
</nav> <!-- end of navbar -->
<!-- end of navigation -->


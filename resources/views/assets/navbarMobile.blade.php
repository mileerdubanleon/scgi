
<style>
    /* Estilos generales para el navbar */
    .navbarr {
        transition: transform 0.5s ease-in-out;
        }

        /* Estilos específicos para dispositivos móviles (hasta 768px) */
        @media (max-width: 768px) {

        .navbar-mobile {
            position: fixed;
            bottom: 0;
            height: 60px;
            width: 100%;
            z-index: 1000;
        }

        .navbar-mobile.hide {
            transform: translateY(56px);
        }

        .item-nav:hover{
            background: #00bfd8
        }

        .item-nav{
            background-color: #00bfd8;
            border: 2px solid #00bfd8;
        }

        .text-btn{
            color: #f3f3f3;
            font-size: 1.6em;
        }
    }
</style>

<!-- Tu código de navbar existente aquí -->
<div class="d-md-none">
    <nav class="navbarr navbar-mobile d-flex">
        <a href="#services"  class=" item-nav col">
            <i class="fas fa-briefcase text-btn my-3"></i>
        </a>
        <a href="#about"  class="item-nav col">
            <i class="fas fa-address-card text-btn my-3"></i>
        </a>

        @if (Auth::check())
        <div class="item-nav col dropup">
            <div class="dropdown-toggle page-scroll my-1" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{-- img profile --}}
                @if(Auth::user()->avatar)
                    <img src="{{ asset('/uploads_user/' . Auth::id() . '/' . Auth::user()->avatar) }}" class="shadow" style="width: 50px; height: 50px; border-radius: 50px; border: 2px solid #27aef7;" alt="Avatar">
                @else
                    <img src="{{ asset('/static/images/default-avatar.png') }}" style="width: 50px; height: 50px; border-radius: 50px;" alt="Default Avatar">
                @endif
            </div>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ url('/account/edit') }}"><span class="item-text lead">Editar perfil</span></a>
                <div class="dropdown-items-divide-hr"></div>
                @if (Auth::user()->role == 1 || Auth::user()->role == 2)
                    <a class="dropdown-item my-3" href="{{ url('/admin') }}"><span class="item-text">Administración</span></a>
                @endif
                <a class="dropdown-item" href="{{ url('/logout') }}"><span class="item-text">Cerrar sesión</span></a>
            </div>
        </div>
        @else

            <a href="{{ url('/login') }}" class="item-nav col">
                <i class="fas fa-user text-btn my-3 shadow"></i>
            </a>

        @endif
            <a href="#contact"  class="item-nav col">
                <i class="fas fa-images text-btn my-3"></i>
            </a>
            <a href=""  class="item-nav col">
                <i class="fas fa-map text-btn my-3"></i>
            </a>
        {{-- end nav --}}
    </nav>
</div>
<script>
    
    let lastScrollTop = 0;
    
    window.addEventListener("scroll", function () {
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        if (scrollTop > lastScrollTop) {
        document.querySelector(".navbar-mobile").classList.add("hide");
        } else {
        document.querySelector(".navbar-mobile").classList.remove("hide");
        }
        lastScrollTop = scrollTop;
    });
</script>
    
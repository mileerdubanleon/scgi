{{-- styles --}}
<style>
    .header {
        @if($setting && !is_null($setting->image_background))
            background: url('{{ asset('storage/' . $setting->image_background) }}') center center no-repeat;
        @else
            background: url('{{ asset('ruta_a_tu_imagen_predeterminada.jpg') }}') center center no-repeat;
        @endif
        background-size: cover;
    }

    @media screen and (max-width: 768px) {
        /* Estilos específicos para pantallas con un ancho máximo de 768 píxeles */
        .header{
            background: none;
        }
    }
</style>

<!-- Header: class header -> img background -->
<header id="header" class="header">  
    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pb-5 mb-3">
                    <div>
                        @if($setting && !is_null($setting->image_background_movile))
                            <img src="{{ asset('storage/' . $setting->image_background_movile) }}" class="w-100 img-window d-md-none" alt="Imagen de fondo">
                        @else
                            <!-- Coloca aquí algún contenido alternativo o un mensaje en caso de que no haya una imagen de fondo móvil -->
                            <span>No hay una imagen de fondo móvil cargada</span>
                        @endif
                    </div>
                    <div class="text-container">
                        <h1>
                            <span class="turquoise title-home">
                                {{ $setting && !is_null($setting->title_background) ? $setting->title_background : 'Default Title' }}
                                <br>
                            </span> 
                        </h1>
                        <p class="p-large mx-3 lead">
                            {{ $setting && !is_null($setting->description_background) ? $setting->description_background : 'Default Description' }}
                        </p>
                    </div>

                    {{-- bottom --}}
                    <div class="mt-5 pt-5 d-flex section-bottom">
                        <span class="d-flex px-5 mx-4 my-1">Contáctenos </span>
                        <a class="btn-solid-lg page-scroll mb-4" href="#contact">></a>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of header-content -->
</header> <!-- end of header -->
<!-- end of header -->
@extends('connect.master')

@section('title', 'Register')

@section('content')


<!--Alert--->
@if(Session::has('message'))
		<div class="position-absolute d-flex justify-content-center text-align-center w-100">
			<div class="mtop16 alert alert-{{ Session::get('typealert') }}" style="display:none;">
				{{ Session::get('message') }}
				@if ($errors->any())
				<ul>
					@foreach($errors->all() as $error)
					<li> {{ $error }} </li>
					@endforeach
				</ul>
				@endif
				<script>
					$('.alert').slideDown();
					setTimeout(function(){ $('.alert').slideUp(); }, 10000)
				</script>
			</div>
		</div>
@endif
<!-- Preloader -->
<div class="spinner-wrapper">
	<div class="spinner">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>
</div>
<!-- end of preloader -->

{{-- <img src="{{ asset('../static/images/wave.png') }}" class="wave" alt="alternative"> --}}
<div class="container">
	<div class="img">
		<img src="{{ asset('../static/images/carpeta.png') }}" class="w-75" alt="alternative">
	</div>
	<div class="login-content">
		{!! Form::open(['url' => '/register']) !!}

			@csrf
			<img src="{{ asset('../static/images/logo.svg') }}" class="my-2" alt="alternative">

			<h2 class="title">Registrate</h2>
			<div class="d-flex form-register">
				<div class="input-div one col-md-6">
					<div class="i">
						<i class="fas fa-pencil-alt"></i>
					</div>
					<div class="div">
						<h5>Nombre</h5>
						{!! Form::text('name', null, ['class' => 'input', 'required']) !!}
					</div>
				</div>
				<div class="input-div one col-md-6">
					<div class="i">
						<i class="fas fa-pencil-alt"></i>
					</div>
					<div class="div">
						<h5>Apellidos</h5>
						{!! Form::text('lastname', null, ['class' => 'input', 'required']) !!}
					</div>
				</div>
			</div>
			<div class="input-div one">
				<div class="i">
					<i class="fas fa-pencil-alt"></i>
				</div>
				<div class="div">
					<h5>Correo</h5>
					{!! Form::email('email', null, ['class' => 'input', 'required']) !!}
				</div>
			</div>
			<div class="input-div one">
				<div class="i">
					<i class="fas fa-lock"></i>
				</div>
				<div class="div">
					<h5>Contraseña</h5>
					{!! Form::password('password', ['class' => 'input', 'required']) !!}
				</div>
			</div>
			<div class="input-div one">
				<div class="i">
					<i class="fas fa-lock"></i>
				</div>
				<div class="div">
					<h5>Repita su contraseña</h5>
					{!! Form::password('cpassword', ['class' => 'input', 'required']) !!}
				</div>
			</div>

			<div class="d-flex my-4 mx-3 gap-3">
				<a href="{{ url('/login') }}">Ya tienes una cuenta. Inicia sesión</a>
			</div>
			{!! Form::submit('Ingresar', ['class' => 'btn', 'value' => 'Login']) !!}


		{!! Form::close() !!}
	</div>
</div>
<script src="{{ asset('static/js/connect/main.js') }}"></script> <!-- Custom scripts -->


@stop
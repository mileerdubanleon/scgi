@extends('admin.master')

@include('admin.navBar')

<!-- jQuery -->
<script src="{{ asset('static/js/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('static/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<div class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header px-4">
                <div class="bg-white mx-2 rounded shadow py-4">
                    <div class="container mt-3 px-5">
                        <h1 class="py-5">Sección para consulta de cotizaciones</h1>

                        <!-- Agregar formulario de búsqueda -->
                        <form action="{{ route('consultaQuotations') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="nombre_cliente">Nombre del Cliente:</label>
                                <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" required>
                            </div>
                            <button type="submit" class="btn btn-primary" id="btnBuscar">Buscar Cotizaciones</button>
                        </form>

                        <!-- Mostrar detalles del cliente -->
                        <div class="row container" id="cliente-details">
                            <p class="mx-2" id="address"></p>
                            <p class="mx-2" id="email"></p>
                            <p class="mx-2" id="phone"></p>
                        </div>

                        <!-- Mostrar cotizaciones aquí -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Cotización ID</th>
                                                <th>items</th>
                                            </tr>
                                        </thead>
                                        <tbody id="cotizacionesContainer">
                                            <!-- Contenido de la tabla generado dinámicamente -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

{{-- Agrega este script al final de tu vista --}}
<script>
    var cotizacionesContainer = $('#cotizaciones-container'); // Mueve esto fuera de la función submit

    $('form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function (response) {
                if (response.errors) {
                    // Manejar errores
                    console.error(response.errors);
                } else {
                    // Mostrar detalles del cliente
                    $('#address').text(`Dirección: ${response.clientDetails.address}`);
                    $('#email').text(`E-mail: ${response.clientDetails.email}`);
                    $('#phone').text(`Teléfono: ${response.clientDetails.phone}`);

                    // Mostrar cotizaciones en #cotizaciones-container
                    cotizacionesContainer.empty();

                    if (response.cotizaciones.length > 0) {
                        $.each(response.cotizaciones, function (index, cotizacion) {
                            var cotizacionRow = $('<tr>');

                            cotizacionRow.append('<td><strong>ID: ' + cotizacion.id + '</strong></td>');
                            cotizacionRow.append('<td></td>'); // Dejar espacio en blanco para los botones

                            $('#cotizacionesContainer').append(cotizacionRow);

                            // Mostrar detalles de los ítems asociados a la cotización
                            $.each(cotizacion.details_quotations, function (detalleIndex, detalle) {
                                var detalleRow = $('<tr>');

                                detalleRow.append('<td></td>'); // Dejar espacio en blanco para el ID de cotización
                                detalleRow.append('<td><strong>Item ID: ' + detalle.item.id + '</strong></td>');

                                // Agregar más detalles según sea necesario

                                $('#cotizacionesContainer').append(detalleRow);
                            });

                            // Agregar una fila adicional para los botones después de los detalles de los ítems
                            var accionesRow = $('<tr>');
                            var accionesCell = $('<td colspan="2">');

                            accionesCell.append('<div class="">' +
                                                '<a class="btn btn-danger mx-1 btn-generar-pdf" href="{{ url('/admin/quotationPDF/') }}/' + cotizacion.id + '/pdf">PDF</a>' +
                                                '<button class="btn btn-info mx-2 btn-ver-pdf my-2" data-cotizacion="' + cotizacion.id + '">Ver PDF</button>' +
                                                '<button class="btn btn-success mx-1 btn-wpp" data-cotizacion="' + cotizacion.id + '">WhatsApp</button>' +
                                                '</div>');

                            accionesRow.append(accionesCell);
                            $('#cotizacionesContainer').append(accionesRow);
                        });
                    } else {
                        cotizacionesContainer.append('<div>No hay cotizaciones disponibles para este cliente.</div>');
                    }

                    console.log(cotizacionesContainer);
                }
            },
            error: function (xhr, status, error) {
                console.error("AJAX error: " + status, xhr, error);
            }
        });
    });

    // deshabilitar boton
    $('form').submit(function (e) {
    e.preventDefault();

    var btnBuscar = $('#btnBuscar');

    // Deshabilitar el botón durante la consulta
    btnBuscar.prop('disabled', true);

    $.ajax({
        // ... (tu código AJAX)

        success: function (response) {
            if (response.errors) {
                // Manejar errores
                console.error(response.errors);
            } else {
                // Ocultar el botón después de una consulta exitosa
                btnBuscar.hide();

                // Resto del código
            }
        },

        error: function (xhr, status, error) {
            console.error("AJAX error: " + status, xhr, error);
        },

        complete: function () {
            // Habilitar el botón después de que la consulta se haya completado (ya sea con éxito o error)
            btnBuscar.prop('disabled', false);
        }
    });
});

</script>
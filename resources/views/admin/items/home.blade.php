@extends('admin.master')

@include('admin.navBar')


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="hold-transition sidebar-mini">
	<div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header px-4">
                <div class="bg-white mx-2 rounded shadow">
                    <form action="{{ route('guardarCotizacionItem') }}" method="POST" class="guardarCotizacion" id="guardarCotizacionForm" name="guardarCotizacionForm">
                        @csrf
                        
                        <div class="container">
                            <div id="print-area">

                                <div class="row mx-3">
                                    <div class="col-lg-6 col-md-6 col-sm-6 my-4">
                                        <h2>Detalles del cliente:</h2>
                                        <select class="cliente form-control" name="cliente" id="cliente" required>
                                            <option value="">Selecciona el cliente</option>
                                        </select>
                                        <span id="direccion"></span>
                                        <div class="my-3">
                                            <span id="address" class="row mx-4"></span>
                                            <span id="email" class="row mx-4"></span>
                                            <span id="phone" class="row mx-4"></span>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 my-4">
                                        <h2>Detalles de la compañía:</h2>
                                        <div style="display: flex; margin-bottom: 50px;">
                                            <div class="salesperson-details">
                                                <span style="font-weight: 500;">Vendedor: <span style="font-weight: 0;">Luis Fernando Bedoya</span></span>
                                                <br>
                                                <span style="font-weight: 500; font-size: .9em;">Cel. 
                                                    <span style="font-weight: 0;">(+507) 6079-1022</span>
                                                </span>
                                                <span style="font-weight: 500; font-size: .9em;">Tel. 
                                                    <span style="font-weight: 0;">240-5054
                                                    </span>
                                                </span>
                                                <br>
                                                    <span style="font-weight: 500;">Correo: </span> 
                                                    <span>alumartpanama@gmail.com</span>
                                                <br>
                                                    <span style="font-weight: 500;">Direccón: </span> 
                                                    <span class="width: 10px !important;">Panamá Oeste, La Paz de Chame</span>
                                                    <br>
                                                    <span>a espaldas del Edificio Alfolí diagonal Comasa</span>
                                                    <br>
                                                    <span> vía Panamericana.</span>
                                            </div>
                                        </div>
                                        <!-- Agrega los campos ocultos para los datos de los ítems -->
                                        <input type="hidden" name="total_general" id="total_general" value="0">
                                        <input type="hidden" name="items_data" id="items_data" value="">
                                        <input type="hidden" name="items" id="hidden_items" value="">
                                    </div>
                                </div>
                                            
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="table-responsive px-3">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class=''>Cantidad</th>
                                                        <th>Descripción</th>
                                                        <th>Ancho</th>
                                                        <th>Alto</th>
                                                        <th>B/. Precio unitario</th>
                                                        <th>B/. Precio total</th>
                                                        <th>Eliminar</th>
                                                        <th class='text-right'></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="items"></tbody>
                                            </table>

                                            <div class="border-bottom my-3"></div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- footer --}}
                                        <div class="d-flex col-md-12">
                                            <div class="col-md-8 p-3" style="width: 60%;  padding: 10px; border-radius: 3px;">
                                            </div>
                                        
                                            <div class="col-md-4" style="">
                                                <div style="border: 1px solid #00000067; padding: 10px; border-radius: 3px;">
                                                    <div class="summary-row summary-row-subtotal" style="font-size: 20px;">
                                                        <span style="font-weight: 600;">SUBTOTAL:</span>
                                                        <span>B/. </span>
                                                    </div>
                                                    <div class="summary-row summary-row-itbms" style="font-size: 20px;">
                                                        <span style="font-weight: 600;">ITBMS (7%):</span>
                                                        <span>B/. </span>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="summary-row summary-row-total my-4" style="font-size: 20px;">
                                                    <span style="border: 1px solid #00000067; padding: 8px; border-radius: 5px; margin-left: 10px; font-weight: 600;">Total:</span>
                                                    <span style="border: 1px solid #00000067; padding: 8px; border-radius: 5px;">B/. </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row"> <hr /></div>

                            @if(kvfj(Auth::user()->permissions, 'mostrarItems'))
                                <button type="button" class="btn btn-info btn-sm btn-agregar-item mx-3" data-toggle="modal" data-target="#myModal">
                                    <span class="glyphicon glyphicon-plus"></span> Agregar Ítem
                                </button>
                            @endif

                            <div class="row mx-2">
                                <div class="col-lg-12 col-md-12 col-sm-12 my-4">
                                    <button type="submit" class="btn btn-success guardar-cotizacion-btn">Guardar cotización</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                {{-- modal --}}

                <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="p-3">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Nuevo Ítem</h4>
                            </div>
                            
                            <div class="modal-body">
                                
                                <div class="row">
                                    <div class="col-md-6 my-3">
                                        <label>Cantidad</label>
                                        <input class="form-control" type="number" name="quantity" id="quantity" placeholder="Cantidad" required>
                                    </div>

                                    <div class="col-md-12">
                                        <label>Descripción</label>
                                        <input class="form-control" type="textarea" name="description" id="description" placeholder="Descripción" required>
                                    </div>
                                    
                                </div>
            
                                <div class="row">
                                    <div class="col-md-6 my-3">
                                        <label>Ancho</label>
                                        <input class="form-control" type="number" name="width_quotes" id="width_quotes" placeholder="Ancho" required>
                                    </div>

                                    <div class="col-md-6 my-3">
                                        <label>Alto</label>
                                        <input class="form-control" type="number" name="height_quotes" id="height_quotes" placeholder="Alto" required>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <label>Precio unitario</label>
                                        <input class="form-control" type="number" name="price_unit" id="price_unit" placeholder="Precio" step="0.01" required>
                                    </div>
                                    
                                </div>

                            </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-success"  id="guardar_item_form">Guardar</button>
                        </div>

                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>
</div>
<!-- ./wrapper -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>

<!-- Tu script personalizado -->
<script>
    // Documento listo para ser manipulado
    $(document).ready(function () {

        // Declara una variable global para almacenar el total
        let total = 0;

        // Configuración de Select2 para el campo cliente
        $(".cliente").select2({
            ajax: {
                url: "search-client",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        term: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            minimumInputLength: 2,
            escapeMarkup: function (markup) { return markup; },
            templateResult: formatCliente,
            templateSelection: formatClienteSelection
            }).on('change', function (e) {
                var clienteId = $(this).val();
        
                // Solicitud Ajax para obtener los detalles del cliente
                $.ajax({
                    url: 'get-client-details/' + clienteId,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        // Actualiza los campos con los detalles del cliente
                        $('#address').text('Dirección: ' +data.address);
                        $('#email').text('E-mail: ' + data.email);
                        $('#phone').text('Teléfono: ' + data.phone);
                    },
                    error: function (xhr, status, error) {
                        console.error(error);
                        // Manejo de errores
                    }
                });
        });

        // Función para dar formato al resultado del cliente en Select2
        function formatCliente(cliente) {
            if (cliente.loading) return cliente.text;
    
            var markup = "<div class='select2-result-cliente clearfix'>" +
                "<div class='select2-result-cliente__nombre'>" + cliente.client_name + "</div>" +
                "</div>";
    
            return markup;
        }

        // Función para dar formato a la selección del cliente en Select2
        function formatClienteSelection(cliente) {
            return cliente.client_name || cliente.text;
        }

        // add items
        $("#guardar_item_form").click(function () {
            // Obtén los valores de los campos
            var quantity = $("#quantity").val();
            var description = $("#description").val();
            var width_quotes = $("#width_quotes").val();
            var height_quotes = $("#height_quotes").val();
            var price_unit = $("#price_unit").val();

            // Calcula el precio total
            var totalRow = quantity * price_unit;

            // Genera un identificador único para la fila
            var rowId = Date.now();

            // Agrega una nueva fila a la tabla con los valores ingresados y el precio total
            $("#items").append("<tr id='" + rowId + "'><td>" + quantity + "</td><td>" + description + "</td><td>" + width_quotes + "</td><td>" + height_quotes + "</td><td>" + price_unit + "</td><td class='total-row'>" + totalRow + "</td><td><button class='btn btn-danger btn-sm btn-eliminar-item'>Eliminar</button></td></tr>");

            // Llama a la función para actualizar los totales y los datos de los ítems
            actualizarTotalesYItems();

            // Limpia los campos del formulario modal
            $("#quantity").val("");
            $("#description").val("");
            $("#width_quotes").val("");
            $("#height_quotes").val("");
            $("#price_unit").val("");

            // Cierra el modal
            $("#myModal").modal("hide");
        });

        // Agrega la lógica para eliminar la fila cuando se hace clic en el botón "Eliminar"
        $("#items").on("click", ".btn-eliminar-item", function () {
            var rowId = $(this).closest("tr").attr("id");
            $(this).closest("tr").remove();

            // Llama a la función para actualizar los totales y los datos de los ítems
            actualizarTotalesYItems();
        });

        // Función para actualizar los totales y los datos de los ítems
        function actualizarTotalesYItems() {
            // Reinicia el total general y la lista de ítems
            total = 0;
            var itemsData = [];

            // Itera sobre todas las filas y suma los totales individuales
            $("#items tr").each(function () {
                var quantity = $(this).find("td:eq(0)").text();
                var description = $(this).find("td:eq(1)").text();
                var width_quotes = $(this).find("td:eq(2)").text();
                var height_quotes = $(this).find("td:eq(3)").text();
                var price_unit = $(this).find("td:eq(4)").text();
                var totalRow = $(this).find("td:eq(5)").text();

                total += isNaN(parseFloat(totalRow)) ? 0 : parseFloat(totalRow);

                itemsData.push({
                    quantity: quantity,
                    description: description,
                    width_quotes: width_quotes,
                    height_quotes: height_quotes,
                    price_unit: price_unit
                });
            });

            // Actualiza los totales en la interfaz
            $(".summary-row-subtotal span:last").text(total.toFixed(2));
            $(".summary-row-itbms span:last").text((total * 0.07).toFixed(2));
            $(".summary-row-total span:last").text((total * 1.07).toFixed(2));
            
            // Actualiza el campo oculto con los datos de los ítems como un arreglo
            $("#hidden_items").val(JSON.stringify(itemsData));
        }

        // Antes de enviar el formulario, actualiza el campo total_general
        // Antes de enviar el formulario, actualiza los campos ocultos
        $(".guardar-cotizacion-btn").click(function (event) {
            // Previene el envío del formulario para realizar acciones personalizadas
            event.preventDefault();

            // Calcula el total general
            var total_general = parseFloat($(".summary-row-total span:last").text());

            // Asigna el total general al campo oculto
            $("#total_general").val(total_general.toFixed(2));

            // Actualiza el campo oculto items_data con el número de filas
            var numFilas = $("#items tr").length;
            $("#items_data").val(numFilas);

            // Envía el formulario
            $("form.guardarCotizacion").submit();
        });
        
        // save quotations ajax
        $('#guardarCotizacionForm').submit(function(event) {
            event.preventDefault();

            // Obtener los datos del formulario
            var formData = new FormData(this);

            // Agregar un console.log para verificar los datos que estás enviando
            console.log(formData);

            // Enviar la solicitud AJAX para guardar los datos
            $.ajax({
                type: 'POST',
                url: '{{ route('guardarCotizacionItem') }}',
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    // Manejar la respuesta del servidor
                    if (response.errors) {
                        // Manejar errores de validación
                        $.each(response.errors, function(key, value) {
                            $('#' + key + '-error').html(value[0]);
                        });
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Debes completar el formulario!'
                        });
                    } else if (response.success) {
                        Swal.fire(
                            '¡Buen trabajo!',
                            'Guardado con éxito!',
                            'success'
                        );
                        location.reload(); // Recargar la página
                        // Puedes redirigir o realizar otras acciones después de guardar con éxito
                    }
                },
                error: function(xhr, status, error) {
                    // Manejar errores de la solicitud AJAX
                    console.error(xhr.responseText);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Ocurrió un error al intentar guardar los datos.'
                    });
                }
            });
        });
        // end save quotations

    });
</script>


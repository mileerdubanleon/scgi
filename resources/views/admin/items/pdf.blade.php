<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    {{-- styles --}}
    <style>
        *{
            font-family: Arial, sans-serif;
        }
        .quote-container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
    
        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }
    
        .logo {
            width: 30px;
            height: 60px;
            opacity: 0.8;
        }
    
        .company-info {
            flex-grow: 1;
            margin-left: 20px;
        }
    
        .customer-details {
            flex: 1;
            margin-top: 10px;
        }

        .salesperson-details {
            font-size: .8rem;
            position: absolute;
            right: 10px;
            top: 110px;
        }

        .customer-details,
        .salesperson-details {
            padding: 0 10px; /* Agrega un poco de espacio interno a cada columna */
        }
    
        .details-table {
            width: 100%;
            margin-top: 20px;
            border-collapse: collapse;
        }
    
        .details-table th,
        .details-table td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
    
        .details-table th {
            background-color: #f2f2f2;
        }

        .summary {
            margin-bottom: 20px;
            margin-left: 450px;
        }

        .summary-row {
            display: flex;
            justify-content: flex-end;
            margin-top: 10px;
        }
    </style>
    
    @if ($quote->count() > 0)
        <div class="quote-container">
            <div class="header">
                <img src="static/images/logo-horizontal.svg" alt="Logo" class="logo">
                <div class="company-info">
                    <!-- Información de la empresa -->
                    <!-- Añade aquí tu información de la empresa -->
                    @if($quote->quotation)
                        <span class="code" style="position: absolute; left: 600px; top: 20;">
                            <strong>Cotización</strong>
                            <div>{{ $quote->quotation->id }}</div>
                        </span>
                    @else
                        <div>No hay detalles de cotización disponibles</div>
                    @endif
                </div>
            </div>
    
            {{-- Accede a la relación "quotation" para obtener los detalles de Quotes --}}
            @if($quote->quotation)
                <div style="display: flex; margin-bottom: 20px;">
                    <div class="customer-details">
                        @php
                            $clientData = $quote->quotation->client;
                        @endphp

                        @if($clientData)
                            <div><span style="font-weight: 500;">Cliente:</span> {{ $clientData->client_name }}</div>
                            <div><span style="font-weight: 500;">Direccion: </span> {{ $clientData->address }}</div>
                            <div><span style="font-weight: 500;">Email: </span> {{ $clientData->email }}</div>
                            <div><span style="font-weight: 500;">Telefono: </span> {{ $clientData->phone }}</div>
                        @else
                            <div>No hay detalles de cliente disponibles</div>
                        @endif
                    </div>
                    <!-- Resto del código -->

                    <div class="col-lg-6 col-md-6 col-sm-6 ">
                        <div style="display: flex; margin-bottom: 50px;">
                            <div class="salesperson-details">
                                <span style="font-weight: 500;">Vendedor: <span style="font-weight: 0;">Luis Fernando Bedoya</span></span>
                                <br>
                                <span style="font-weight: 500; font-size: .9em;">Cel. 
                                    <span style="font-weight: 0;">(+507) 6079-1022</span>
                                </span>
                                <span style="font-weight: 500; font-size: .9em;">Tel. 
                                    <span style="font-weight: 0;">240-5054
                                    </span>
                                </span>
                                <br>
                                    <span style="font-weight: 500;">Correo: </span> 
                                    <span>alumartpanama@gmail.com</span>
                                <br>
                                    <span style="font-weight: 500;">Direccón: </span> 
                                    <span class="width: 10px !important;">Panamá Oeste, La Paz de Chame</span>
                                    <br>
                                    <span>a espaldas del Edificio Alfolí diagonal Comasa</span>
                                    <br>
                                    <span> vía Panamericana.</span>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div>No hay detalles de cliente disponibles</div>
            @endif
            <hr>

            <!-- Table data -->
            <table class="details-table">
                <thead>
                    <tr style="font-size: 15px;">
                        <th>Cantidad</th>
                        <th>Descripción</th>
                        <th>Ancho (cm)</th>
                        <th>Alto (cm)</th>
                        <th>Precio Unitario</th>
                        <th>Precio Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($quote->quotation->detailsQuotation as $detailsQuote)
                        <tr style="font-size: 12px;">
                            <td>{{ $detailsQuote->item->quantity }}</td>
                            <td>{{ $detailsQuote->item->description }}</td>
                            <td>{{ $detailsQuote->item->width_quotes }}</td>
                            <td>{{ $detailsQuote->item->height_quotes }}</td>
                            <td>B/. {{ $detailsQuote->item->price_unit }}</td>
                            <td>B/. {{ number_format($detailsQuote->item->quantity * $detailsQuote->item->price_unit, 2) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <!-- Calcular subtotal, IVA y total -->
            @php
                $subtotal = 0;
                $total = 0;

                foreach ($quote->quotation->detailsQuotation as $detailsQuote) {
                    $subtotal += $detailsQuote->item->quantity * $detailsQuote->item->price_unit;
                    $total += $detailsQuote->item->quantity * $detailsQuote->item->price_unit;
                }

                // Calcular IVA (7%)
                $iva = $subtotal * 0.07;

                // Calcular el Total
                $total += $iva;
            @endphp
            <hr style="margin-top: 40px;">

            <!-- Footer -->
            <div style="display: flex; margin-top: 20px;">
                <div style="position:absolute; display: flex; width: 60%; background: rgba(30, 120, 188, 0.476); padding: 10px; border-radius: 3px;">
                    <span style="font-weight: 500;">Observaciones.</span>
                    <span style="font-size: .8em;">Abono inicial del 50%, entrega a satisfacción el 50% restante.</span>
                </div>
                
                <div class="summary" style=" width: 30%; left: 0;">
                    <div style="border: 1px solid #000; padding: 10px; border-radius: 3px;">
                        <div class="summary-row" style="font-size: 12px;">
                            <span>SUBTOTAL:</span>
                            <span>B/. {{ number_format($subtotal, 2) }} </span>
                        </div>
                        <div class="summary-row" style="font-size: 12px;">
                            <span>ITBMS (7%):</span>
                            <span>B/. {{ number_format($iva, 2) }}</span>
                        </div>
                    </div>
                    <hr>
                    <div class="summary-row" style="margin-top: 30px;">
                        <span style="border: 1px solid #00000067; padding: 8px; border-radius: 5px; margin-left: 10px;">Total:</span>
                        <span style="border: 1px solid #00000067; padding: 8px; border-radius: 5px;">B/. {{ number_format($total, 2) }}</span>
                    </div>
                </div>
            </div>

            <div style="width: 100%; justify-content: center; text-align: center; margin-top: 50px;">
                <span style=""><em>"Esperamos tener el gusto de poder servirle"</span>
            </div>

        </div>
    @else
        <div>No hay detalles disponibles</div>
    @endif

</body>
</html>

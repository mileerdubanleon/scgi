<!-- En tu vista (nombre_de_tu_vista.blade.php) -->
<table class="table table-striped table-hover">
    <!-- Cabecera de la tabla -->
    <!-- ... -->

    <tbody class='items' id="datos_factura">
        <!-- Mostrar los items aquí -->
        @foreach($items as $item)
            <tr>
                <td></td>
                <td>{{ $item['quantity'] }}</td>
                <td>{{ $item['description'] }}</td>
                <td>{{ $item['width_quotes'] }}</td>
                <td>{{ $item['height_quotes'] }}</td>
                <td>{{ $item['price_unit'] }}</td>
                <!-- Otras columnas... -->
            </tr>
        @endforeach
    </tbody>
</table>
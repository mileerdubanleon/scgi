@extends('admin.master')

@include('admin.navBar')
<!-- Theme style -->
<link rel="stylesheet" href="../static/css/adminlte.min.css">

{{-- Page --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mx-3 my-4">
                        <div class="">
                            <h1>Dashboard</h1>
                        </div>
                        <div class="mx-4">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                    </div>

                    <div class="row">
                        {{-- Clientes de la pagina --}}
                        @include('admin.dashboard.clients')
                        {{-- end clientes --}}

                        {{-- cotizaciones de la pagina --}}
                        @include('admin.dashboard.quotes')
                        {{-- end  --}}

                        {{-- product de la pagina --}}
                        @include('admin.dashboard.product')
                        {{-- end  --}}

                        {{-- empleado de la pagina --}}
                        @include('admin.dashboard.employee')
                        {{-- end  --}}

                        {{-- proyecto de la pagina --}}
                        @include('admin.dashboard.project')
                        {{-- end  --}}

                        {{--  de la pagina --}}
                        @include('admin.dashboard.users')
                        {{-- end  --}}

                    </div>
                    <!-- /.row -->

                </div><!-- /.container-fluid -->
            </section>
        </div>
    </div>
</div>

<!-- ./wrapper -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- jQuery -->
<script src="../static/js/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

@extends('admin.master')

@include('admin.navBar')


<div class="hold-transition sidebar-mini">
	<div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        
                        <!-- Content Header (Page header) -->
                        <section class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2 mx-3">
                                    <div class="">
                                        <h1>Empleados</h1>
                                    </div>
                                    <div class="mx-4 mt-1">
                                        <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                                        <li class="breadcrumb-item active"><a href="{{ url('/admin/empleados') }}">Empleados</a></li>
                                        <li class="breadcrumb-item"><a href="{{ url('/admin/empleados') }}">Editar empleado:  {{ $employee->name }} {{ $employee->lastname }}</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div><!-- /.container-fluid -->
                        </section>
                        
                        <div class="col-12">
                            <div class="card mt-4">
                                <div class="inside p-5">
                                    {!! Form::open(['url' =>'/admin/empleados/'.$employee->id.'/edit', 'files' => true, 'id' => 'employee-edit']) !!}
                                    @csrf
                                        <div class="row my-2">
                                            <div class="col">
                                                <label for="name">Nombre del empleado: </label>
                                                <div>
                                                    {!! Form::text('name', $employee->name, ['class' => 'form-control']) !!}
                                                </div>
                                                <span class="text-danger" id="name-error"></span>
                                            </div>
                                            <div class="col">
                                                <label for="lastname">Apellido del empleado: </label>
                                                <div>
                                                    {!! Form::text('lastname', $employee->lastname, ['class' => 'form-control']) !!}
                                                </div>
                                                <span class="text-danger" id="lastname-error"></span>
                                            </div>
                                        </div>

                                        <div class="row my-2">
                                            <div class="col">
                                                <label for="type_document">Tipo de documento: </label>
                                                <div>
                                                    {!! Form::text('type_document', $employee->type_document, ['class' => 'form-control', 'disabled']) !!}
                                                </div>
                                                <span class="text-danger" id="type_document-error"></span>
                                            </div>
                                            <div class="col">
                                                <label for="identification">Identificación: </label>
                                                <div>
                                                    {!! Form::number('identification', $employee->identification, ['class' => 'form-control']) !!}
                                                </div>
                                                <span class="text-danger" id="identification-error"></span>
                                            </div>
                                        </div>

                                        <div class="row my-2">
                                            <div class="col">
                                                <label for="date">Fecha: </label>
                                                <div>
                                                    {!! Form::date('date', $employee->date, ['class' => 'form-control']) !!}
                                                </div>
                                                <span class="text-danger" id="date-error"></span>
                                            </div>
                                            <div class="col">
                                                <label for="salary">Salario: </label>
                                                <div>
                                                    {!! Form::number('salary', $employee->salary, ['class' => 'form-control']) !!}
                                                </div>
                                                <span class="text-danger" id="salary-error"></span>
                                            </div>
                                        </div>

                                        <div class="row my-4">
                                            <div class="col-md-12">
                                                {!! Form::submit('Actualizar', ['class' => 'btn btn-success']) !!}
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

		</div>
	</div>
</div>


<!-- jQuery -->
<script src="{{ asset('static/js/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('static/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- ./wrapper -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(document).ready(function() {
        $('#employee-edit').submit(function(event) {
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: '{{ url('/admin/empleados/'.$employee->id.'/edit') }}',
                data: $(this).serialize(),
                success: function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: '¡Buen trabajo!',
                        text: 'Actualizado con éxito.',
                    }).then(function() {
                        // Recargar la página después de cerrar el Sweet Alert
                        location.reload(true);
                    });
                }
            });
        });

        // Eliminar mensajes de error cuando se enfoca en un campo
        $('input, textarea, select').focusout(function() {
            $(this).next('.text-danger').empty();
        });
    });
</script>
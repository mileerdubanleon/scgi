<!-- Modal Add-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar empleado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <!-- Tu formulario aquí -->
            {!! Form::open(['url' => '/admin/empleados/add', 'files' => true, 'id' => 'employee-form']) !!}
            @csrf
            <!-- ... (contenido del formulario) ... -->
                <div class="row my-2">
                    <div class="col">
                        <label for="name">Nombres: </label>
                        <div>
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="name-error"></span>
                    </div>
                    <div class="col">
                        <label for="lastname">Apellidos: </label>
                        <div>
                            {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="lastname-error"></span>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="type_document">Tipo de documento: </label>
                        <div>
                            {!! Form::select('type_document', ['Tarjeta de identidad' => 'Tarjeta de identidad', 
                            'Cédula de ciudadania' => 'Cédula de ciudadania', 
                            'Registro civil' => 'Registro civil', 
                            'Cédula de extranjería' => 'Cédula de extranjería', 
                            'Pasaporte' => 'Pasaporte'
                            ],null, ['class' => 'form-select form-control']) !!}
                        </div>
                    </div>
                    <div class="col">
                        <label for="identification">No. de documento: </label>
                        <div>
                            {!! Form::number('identification', null, ['class' => 'form-control', 'id' => 'identification-input']) !!}
                        </div>
                        <span class="text-danger" id="identification-error"></span>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="date">Fechas de cumpleaños: </label>
                        <div>
                            {!! Form::date('date', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="date-error"></span>
                    </div>

                    <div class="col">
                        <label for="salary">Salario: </label>
                        <div>
                            {!! Form::number('salary', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="salary-error"></span>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-success', 'id' => 'guardar-btn', 'disabled']) !!}
                </div>

            {!! Form::close() !!}
            </div>

            <script>
                $(document).ready(function() {
                    // Verificar el documento en tiempo real
                    $('#identification-input').on('input', function() {
                        var identification = $(this).val();

                        // Eliminar mensajes de error y habilitar el botón
                        $('#identification-error').empty();
                        $('#guardar-btn').prop('disabled', false);

                        // Verificar el correo mediante AJAX
                        $.ajax({
                            type: 'GET',
                            url: '{{ route('employee') }}',
                            data: { identification: identification },
                            success: function(response) {
                                if (response.exists) {
                                    $('#identification-error').html('El correo ya existe.');
                                    $('#guardar-btn').prop('disabled', true);
                                }
                            }
                        });
                    });

                    // enviar formulario
                    $('#employee-form').submit(function(event) {
                        event.preventDefault();
            
                        // Eliminar mensajes de error de todos los campos
                        $('.text-danger').empty();
            
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('employee_add') }}',
                            data: $(this).serialize(),
                            success: function(response) {
                                if (response.errors) {
                                    // Manejar errores de validación
                                    $.each(response.errors, function(key, value) {
                                        $('#' + key + '-error').html(value[0]);
                                    });
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Debes completar el formulario!'
                                    })
                                } else if (response.success) {
                                    Swal.fire(
                                        '¡Buen trabajo!',
                                        'Guardado con éxito!',
                                        'success'
                                    )
                                    location.reload(); // Recargar la página
                                    // Puedes redirigir o realizar otras acciones después de guardar con éxito
                                }
                            }
                        });
                    });
            
                    // Eliminar mensajes de error cuando se enfoca en un campo
                    $('input, textarea, select').focusout(function() {
                        $(this).next('.text-danger').empty();
                    });
                });
            </script>
        </div>
    </div>
</div>

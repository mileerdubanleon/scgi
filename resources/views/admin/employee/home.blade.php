@extends('admin.master')

@include('admin.navBar')

<!-- DataTables -->
<link rel="stylesheet" href="../static/js/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../static/css/adminlte.min.css">

{{-- table --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mx-3">
                    <div class="">
                        <h1>Empleados</h1>
                    </div>
                    <div class="mx-4 mt-1">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Empleados</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                @if(kvfj(Auth::user()->permissions, 'employee_add'))
                                    <div class="card-header">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                            + Agregar Empleado
                                        </button>
                                    </div>
                                @endif
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="font-weight-bold">
                                                <td>ID</td>
                                                <td>Nombres</td>
                                                <td>Apellidos</td>
                                                <td>Tipo de documento</td>
                                                <td>No. Documento</td>
                                                <td>Fecha de cumpleaños</td>
                                                <td>Salario</td>
                                                <td>Estado</td>
                                                <td>Acción</td>
                                            </tr>
                                        </thead>
                                    <tbody>
                                        @foreach($employee as $e)
                                            <tr>
                                                <td>{{ $e->id }}</td>
                                                <td>{{ $e->name }}</td>
                                                <td>{{ $e->lastname}}</td>
                                                <td>{{ $e->type_document }}</td>
                                                <td>{{ $e->identification }}</td>
                                                <td>{{ $e->date }}</td>
                                                <td>B/.{{ $e->salary }}</td>
                                                <td>
                                                    <button class="btn btn-toggle-state {{ $e->state == 0 ? 'btn-danger' : 'btn-success' }}" data-item-id="{{ $e->id }}" data-current-state="{{ $e->state }}">
                                                        @if($e->state == 1)
                                                            <span class="btn-text">Activo</span>
                                                        @else
                                                            <span class="btn-text">Inactivo</span>
                                                        @endif
                                                    </button>
                                                </td>
                                                <td>
                                                    <a href="{{ url('/admin/empleados/'.$e->id.'/edit') }}" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-success mx-2">
                                                        Editar
                                                    </a>
                                                    <a href="{{ url('/admin/empleados/'.$e->id.'/delete') }}" onclick="confirmDelete('{{ $e->id }}', event)" data-path="admin/employee" data-action="delete" data-object="{{ $e->id }}" data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn btn-danger">
                                                        Eliminar
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                        <tfoot>
                                            <tr class="font-weight-bold">
                                                <td>ID</td>
                                                <td>Nombres</td>
                                                <td>Apellidos</td>
                                                <td>Tipo de documento</td>
                                                <td>No. Documento</td>
                                                <td>Fecha de cumpleaños</td>
                                                <td>Salario</td>
                                                <td>Estado</td>
                                                <td>Acción</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="https://adminlte.io">Mileer león</a>.</strong> All rights reserved.
        </footer>

	</div>
	<!-- ./wrapper -->

	<!-- jQuery -->
	<script src="../static/js/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ./wrapper -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    {{-- axios for btn state --}}
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });

        // sweet alert
        function confirmDelete(id, event) {
            event.preventDefault();  // Evitar el comportamiento predeterminado del enlace
            
            Swal.fire({
                title: '¿Estás seguro?',
                text: 'Esta acción no se puede deshacer',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Sí, eliminar'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ url("/admin/empleados") }}/' + id + '/delete';
                }
            });
        }

        // btn state
        document.addEventListener('DOMContentLoaded', function () {
            const buttons = document.querySelectorAll('.btn-toggle-state');

            buttons.forEach(button => {
                button.addEventListener('click', function () {
                    const itemId = this.dataset.itemId;
                    const currentState = this.dataset.currentState;

                    // Realiza una solicitud al servidor para cambiar el estado
                    axios.post('/admin/cambiarEstadoEmployee', { itemId, currentState })
                        .then(response => {
                            // Actualiza la interfaz con el nuevo estado devuelto por el servidor
                            const newState = response.data.newState;
                            this.dataset.currentState = newState;

                            // Actualiza el texto en el botón
                            const btnText = this.querySelector('.btn-text');
                            btnText.textContent = newState === '1' ? 'Activo' : 'Inactivo';

                            // Actualiza las clases del botón según el nuevo estado
                            this.classList.remove(newState === '1' ? 'btn-danger' : 'btn-success');
                            this.classList.add(newState === '1' ? 'btn-success' : 'btn-danger');
                        })
                        .catch(error => {
                            console.error('Error al cambiar el estado:', error);
                        });
                });
            });
        });
	</script>
</div>

@include('admin.employee.modalAdd')



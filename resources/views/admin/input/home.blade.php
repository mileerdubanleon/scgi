@extends('admin.master')

@include('admin.navBar')

<!-- DataTables -->
<link rel="stylesheet" href="../static/js/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../static/css/adminlte.min.css">

<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mx-3">
                    <div class="">
                        <h1>Entradas</h1>
                    </div>
                    <div class="mx-4 mt-1">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                        <li class="breadcrumb-item active">Entradas</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                @if(kvfj(Auth::user()->permissions, 'inputAdd'))
                                    <div class="card-header">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                            + Agregar entrada
                                        </button>
                                    </div>
                                @endif
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="font-weight-bold">
                                                <td>ID</td>
                                                <td>Nombre producto</td>
                                                <td>Cantidad</td>
                                                <td>Fecha</td>
                                                <td>Precio Und</td>
                                                <td>Total B/.</td>
                                                <td>Acción</td>
                                            </tr>
                                        </thead>
                                    <tbody>
                                        @foreach($input as $in)
                                            <tr>
                                                <td>{{ $in->id }}</td>
                                                <td>
                                                    @if ($in->inventory)
                                                        {{ $in->inventory->name }} 
                                                        {{ $in->inventory->codigo }}
                                                    @else
                                                        No disponible
                                                    @endif
                                                </td>
                                                <td>{{ $in->quantity}}</td>
                                                <td>{{ $in->created_at }}</td>
                                                <td>{{ $in->inventory ? $in->inventory->price : 'No disponible' }}</td>
                                                <td>{{ $in->inventory ? $in->inventory->price * $in->quantity : 'No disponible' }}</td>
                                                <td width="160">
                                                    @if(kvfj(Auth::user()->permissions, 'input_delete'))
                                                        <a href="#" onclick="confirmDelete('{{ $in->id }}', event)" data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn btn-danger">
                                                            Eliminar
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                        <tfoot>
                                            <tr class="font-weight-bold">
                                                <td>ID</td>
                                                <td>Nombre producto</td>
                                                <td>Cantidad</td>
                                                <td>Fecha</td>
                                                <td>Precio Und</td>
                                                <td>Total B/.</td>
                                                <td>Acción</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="#">Mileer león</a>.</strong> All rights reserved.
        </footer>

	</div>

    <!-- ./wrapper -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<!-- jQuery -->
	<script src="../static/js/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    
	<script>
        // buttons datatable
        $(function () {
            $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });

        // sweet alert
        function confirmDelete(id, event) {
            event.preventDefault();  // Evitar el comportamiento predeterminado del enlace
            
            Swal.fire({
                title: '¿Estás seguro?',
                text: 'Esta acción no se puede deshacer',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Sí, eliminar'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ url("/admin/input") }}/' + id + '/delete';
                }
            });
        }
	</script>

</div>

@include('admin.input.modalAdd')
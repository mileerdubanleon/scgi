
<div class="p-3 bg-white px-4">
    <div class="px-4 py-4">
        <span class="h4">Creando cotización</span>
    </div>
    <!-- Tu formulario aquí -->
    {!! Form::open(['url' => '/admin/detallesCotizaciones/add', 'files' => true, 'id' => 'detailsQuotes-form']) !!}
    @csrf
        <div class="row">
            <div class="col-md-2">
                <label for="codigo">No. De cotización: </label>
                    <div>
                        {!! Form::text('codigo', '', ['class' => 'form-control', 'id' => 'codigo']) !!}
                    </div>
                <span class="text-danger" id="codigo-error"></span>
                <div id="generatedCode"></div> <!-- Aquí se mostrará el número de cotización generado -->
            </div>

            <div class="col-md-3">
                <label for="details_quotes">Cliente: </label>
                    <div>
                        {!! Form::select('details_quotes', $clients, null, ['class' => 'form-control', 'id' => 'client-select', 'placeholder' => 'Seleccione un cliente']) !!}
                    </div>
                <span class="text-danger" id="details_quotes-error"></span>
            </div>
        </div>
        <div class="row my-4">

            <div class="col-md-1">
                <label for="quantity">Cantidad: </label>
                    <div>
                        {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
                    </div>
                <span class="text-danger" id="quantity-error"></span>
            </div>

            <div class="col">
                <label for="description">Descripción: </label>
                    <div>
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'style' => 'height: 80px; max-height: 200px;']) !!}
                    </div>
                <span class="text-danger" id="description-error"></span>
            </div>

            <div class="col-md-1">
                <label for="width_quotes">Ancho: </label>
                    <div>
                        {!! Form::text('width_quotes', null, ['class' => 'form-control']) !!}
                    </div>
                <span class="text-danger" id="width_quotes-error"></span>
            </div>

            <div class="col-md-1">
                <label for="height_quotes">Alto: </label>
                    <div>
                        {!! Form::text('height_quotes', null, ['class' => 'form-control']) !!}
                    </div>
                <span class="text-danger" id="height_quotes-error"></span>
            </div>

            <div class="col-md-2">
                <label for="price_unit">Precio Unit: </label>
                    <div>
                        {!! Form::text('price_unit', null, ['class' => 'form-control']) !!}
                    </div>
                <span class="text-danger" id="price_unit-error"></span>
            </div>

            {{-- Button plus add quantity --}}
            <div class="col-md-1 d-flex justify-content-center align-items-center">
                {!! Form::submit('+', ['class' => 'w-75 h-50 shadow btn-hover']) !!}
            </div>
        </div>
    {!! Form::close() !!}

    <hr>

</div>


<!-- jQuery -->
<script src="../static/js/plugins/jquery/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        $('#detailsQuotes-form').submit(function(event) {
            event.preventDefault();

            // Eliminar mensajes de error de todos los campos
            $('.text-danger').empty();

            $.ajax({
                type: 'POST',
                url: '{{ route('details_quotes_add') }}',
                data: $(this).serialize(),
                success: function(response) {
                    if (response.errors) {
                        // Manejar errores de validación
                        $.each(response.errors, function(key, value) {
                            $('#' + key + '-error').html(value[0]);
                        });
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Debes completar el formulario!'
                        })
                    } else if (response.success) {
                        Swal.fire(
                            '¡Buen trabajo!',
                            'Guardado con éxito!',
                            'success'
                        )
                        location.reload(); // Recargar la página
                        // Puedes redirigir o realizar otras acciones después de guardar con éxito
                    }
                }
            });
        });

        // Eliminar mensajes de error cuando se enfoca en un campo
        $('input, textarea, select').focusout(function() {
            $(this).next('.text-danger').empty();
        });
    });
</script>


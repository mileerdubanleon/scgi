
@extends('admin.master')

@include('admin.navBar')

<!-- DataTables -->
<link rel="stylesheet" href="../static/js/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">


{{-- table --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2 mx-3">
                        <div class="">
                            <h1>Sección de cotizaciones</h1>
                        </div>
                        <div class="mx-4 mt-1">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                            <li class="breadcrumb-item active">Tabla / Cotizaciones</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="clientQuotesTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="font-weight-bold">
                                                <td>Codigo. Cotización</td>
                                                <td>Fecha de creación</td>
                                                <td>Descargar Cotización</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($tableQuotes->count() > 0)
                                                @php
                                                    $uniqueCodes = $tableQuotes->unique('codigo');
                                                @endphp

                                                @foreach ($uniqueCodes as $uniqueCode)
                                                    @php
                                                        $firstQuote = $tableQuotes->where('codigo', $uniqueCode->codigo)->first();
                                                    @endphp
                                                    <tr>
                                                        <td>{{ $firstQuote->codigo }}</td>
                                                        <td>{{ $firstQuote->created_at }}</td>
                                                        <td>
                                                            <a href="{{ url('/admin/tableCotizaciones/'.$firstQuote->codigo.'/pdf') }}" class="btn btn-outline-danger open-quotes">Descargar PDF</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="3">No hay cotizaciones disponibles</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr class="font-weight-bold">
                                                <td>Codigo. Cotización</td>
                                                <td>Fecha de creación</td>
                                                <td>Descargar Cotización</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="">Mileer león</a>.</strong> All rights reserved.
        </footer>

	</div>
	<!-- ./wrapper -->

	<!-- jQuery -->
		<script src="{{ asset('static/js/plugins/jquery/jquery.min.js') }}"></script>
		<!-- jQuery UI 1.11.4 -->
		<script src="{{ asset('static/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

</div>
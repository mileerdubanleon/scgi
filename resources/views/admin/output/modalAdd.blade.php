<!-- Modal Add-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar salida</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Tu formulario aquí -->
                {!! Form::open(['url' => '/admin/output/add', 'files' => true, 'id' => 'output-form']) !!}
                @csrf
                <!-- ... (contenido del formulario) ... -->
                    <div class="row my-2">
                        <div class="col">
                            <label for="name_inventory_output">Producto: </label>
                            <div>
                                {!! Form::select('name_inventory_output', $namiven, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col">
                            <label for="output_employee">Empleado: </label>
                            <div>
                                {!! Form::select('output_employee', $ouemp, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="col">
                            <label for="codigo">Cantidad: </label>
                            <div>
                                {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
                            </div>
                            
                            <span class="text-danger" id="quantity-error"></span>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                    </div>

                {!! Form::close() !!}
            </div>

            <script>
                $(document).ready(function() {
                    $('#output-form').submit(function(event) {
                        event.preventDefault();
            
                        // Eliminar mensajes de error de todos los campos
                        $('.text-danger').empty();
            
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('output_add') }}',
                            data: $(this).serialize(),
                            success: function(response) {
                                if (response.errors) {
                                    // Manejar errores de validación
                                    $.each(response.errors, function(key, value) {
                                        $('#' + key + '-error').html(value[0]);
                                    });
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Debes completar el formulario!'
                                    })
                                } else if (response.success) {
                                    Swal.fire(
                                        '¡Buen trabajo!',
                                        'Guardado con éxito!',
                                        'success'
                                    )
                                    location.reload(); // Recargar la página
                                    // Puedes redirigir o realizar otras acciones después de guardar con éxito
                                }
                            }
                        });
                    });
            
                    // Eliminar mensajes de error cuando se enfoca en un campo
                    $('input, textarea, select').focusout(function() {
                        $(this).next('.text-danger').empty();
                    });
                });
            </script>
        </div>
    </div>
</div>
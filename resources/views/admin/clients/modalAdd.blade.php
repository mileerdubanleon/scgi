<!-- Modal Add-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Tu formulario aquí -->
                {!! Form::open(['url' => '/admin/clients/add', 'files' => true, 'id' => 'client-form']) !!}
                @csrf
                <!-- ... (contenido del formulario) ... -->
                <div class="row my-2">
                    <div class="col">
                        <label for="client_name">Nombre: </label>
                        <div>
                            {!! Form::text('client_name', null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="client_name-error"></span>
                    </div>
                    <div class="col">
                        <label for="lastname">Dirección: </label>
                        <div>
                            {!! Form::text('address', null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="address-error"></span>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="email">Correo: </label>
                        <div>
                            {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email-input']) !!}
                        </div>
                        <span class="text-danger" id="email-error"></span>
                    </div>
                    <div class="col">
                        <label for="phone">Teléfono: </label>
                        <div>
                            {!! Form::number('phone', null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="phone-error"></span>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-success', 'id' => 'guardar-btn', 'disabled']) !!}
                </div>

                {!! Form::close() !!}
            </div>

            {{-- script add data clients --}}
            <script>
                $(document).ready(function() {
                    // Verificar el correo en tiempo real
                    $('#email-input').on('input', function() {
                        var email = $(this).val();

                        // Eliminar mensajes de error y habilitar el botón
                        $('#email-error').empty();
                        $('#guardar-btn').prop('disabled', false);

                        // Verificar el correo mediante AJAX
                        $.ajax({
                            type: 'GET',
                            url: '{{ route('clients') }}',
                            data: { email: email },
                            success: function(response) {
                                if (response.exists) {
                                    $('#email-error').html('El correo ya existe.');
                                    $('#guardar-btn').prop('disabled', true);
                                }
                            }
                        });
                    });

                    // Enviar el formulario
                    $('#client-form').submit(function(event) {
                        event.preventDefault();

                        // Eliminar mensajes de error de todos los campos
                        $('.text-danger').empty();

                        $.ajax({
                            type: 'POST',
                            url: '{{ route('client_add') }}',
                            data: $(this).serialize(),
                            success: function(response) {
                                if (response.errors) {
                                    // Manejar errores de validación
                                    $.each(response.errors, function(key, value) {
                                        $('#' + key + '-error').html(value[0]);
                                    });
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Debes completar el formulario!'
                                    })
                                } else if (response.success) {
                                    Swal.fire(
                                        '¡Buen trabajo!',
                                        'Guardado con éxito!',
                                        'success'
                                    )
                                    location.reload(); // Recargar la página
                                    // Puedes redirigir o realizar otras acciones después de guardar con éxito
                                }
                            }
                        });
                    });

                    // Eliminar mensajes de error cuando se enfoca en un campo
                    $('input, textarea, select').focusout(function() {
                        $(this).next('.text-danger').empty();
                    });
                });
            </script>
        </div>
    </div>
</div>
@extends('admin.master')

@include('admin.navBar')


<div class="hold-transition sidebar-mini">
	<div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        
                        <!-- Content Header (Page header) -->
                        <section class="content-header">
                            <div class="container-fluid">
                                <div class="row mb-2 mx-3">
                                    <div class="">
                                        <h1>Clientes</h1>
                                    </div>
                                    <div class="mx-4 mt-1">
                                        <ol class="breadcrumb float-sm-right">
                                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                                        <li class="breadcrumb-item active"><a href="{{ url('/admin/clientes') }}">Clientes</a></li>
                                        <li class="breadcrumb-item active"><a href="{{ url('/admin/clients/'.$client->id.'/edit') }}">Editar cliente:  {{ $client->client_name }}</a></li>
                                        </ol>
                                    </div>
                                </div>
                            </div><!-- /.container-fluid -->
                        </section>
                        
                        <div class="col-12">
                            <div class="card mt-4">
                                <div class="inside p-5">
                                    {!! Form::open(['url' =>'/admin/clients/'.$client->id.'/edit', 'files' => true, 'id' => 'clients-edit']) !!}
                                    @csrf
                                        <div class="row my-2">
                                            <div class="col">
                                                <label for="client_name">Nombre del cliente: </label>
                                                <div>
                                                    {!! Form::text('client_name', $client->client_name, ['class' => 'form-control', 'required']) !!}
                                                </div>
                                                <span class="text-danger" id="name-error"></span>
                                            </div>
                                            <div class="col">
                                                <label for="address">Dirección: </label>
                                                <div>
                                                    {!! Form::text('address', $client->address, ['class' => 'form-control', 'required']) !!}
                                                </div>
                                                <span class="text-danger" id="address-error"></span>
                                            </div>
                                        </div>

                                        <div class="row my-2">
                                            <div class="col">
                                                <label for="email">Correo: </label>
                                                <div>
                                                    {!! Form::email('email', $client->email, ['class' => 'form-control', 'step' => 'any', 'required']) !!}
                                                </div>
                                                <span class="text-danger" id="email-error"></span>
                                            </div>
                                            <div class="col">
                                                <label for="phone">Teléfono: </label>
                                                <div>
                                                    {!! Form::number('phone', $client->phone, ['class' => 'form-control', 'step' => 'any', 'required']) !!}
                                                </div>
                                                <span class="text-danger" id="phone-error"></span>
                                            </div>
                                        </div>

                                        <div class="row my-4">
                                            <div class="col-md-12">
                                                {!! Form::submit('Actualizar', ['class' => 'btn btn-success']) !!}
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

		</div>
	</div>
</div>


<!-- jQuery -->
<script src="{{ asset('static/js/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('static/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- ./wrapper -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(document).ready(function() {
        $('#clients-edit').submit(function(event) {
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: '{{ url('/admin/clients/'.$client->id.'/edit') }}',
                data: $(this).serialize(),
                success: function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: '¡Buen trabajo!',
                        text: 'Actualizado con éxito.',
                    }).then(function() {
                        // Recargar la página después de cerrar el Sweet Alert
                        location.reload(true);
                    });
                }
            });
        });

        // Eliminar mensajes de error cuando se enfoca en un campo
        $('input, textarea, select').focusout(function() {
            $(this).next('.text-danger').empty();
        });
    });
</script>
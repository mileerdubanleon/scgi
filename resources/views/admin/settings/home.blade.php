@extends('admin.master')

@include('admin.navBar')
{{-- table --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            {{-- message errors --}}
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <div class="container-fluid">
                    <div class="row mb-2 mx-3">
                        <div class="">
                            <h1>Imagenes de inicio</h1>
                        </div>
                        <div class="mx-4 mt-1">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                            <li class="breadcrumb-item active"><i class="fas fa-user-cog"></i> Imagenes de inicio</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {{-- form images --}}
                                            {{-- form text --}}
                                            {!! Form::open(['url' => '/admin/settings/add', 'id' => 'form_settings_change', 'files' => true]) !!}
                                                @csrf
                                                {{-- sections change images --}}
                                                <div class="row">
                                                    {{-- card title section --}}
                                                    <span class="mx-2 my-3 lead bg-info rounded p-2 col-md-12">
                                                        Esta sección se hizo con el fin de cambiar las imágenes de fondo tanto en tamaño web como en móvil:
                                                    </span>
                                                    <!-- Imagen de inicio -->
                                                    <div class="col-md-6 col-12">
                                                        <span class="bg-warning rounded shadow-sm h5 px-2 my-1 text-white">Cambiar imagen de inicio: </span>
                                                        <div class="card my-4">
                                                            <label for="webImageInput" class="image-input-label">
                                                                <div class="image-container">
                                                                    <!-- Vista previa de la imagen actual -->
                                                                    <div id="webImagePreview">
                                                                        @if($setting->isEmpty() || is_null($setting[0]->image_background))
                                                                            <span class="p-4">No hay una imagen cargada</span>
                                                                        @else
                                                                            <img src="{{ asset('storage/' . $setting[0]->image_background) }}" class="image-preview" alt="Imagen de fondo">
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <!-- Input para seleccionar el archivo -->
                                                                <input hidden type="file" name="image_background" id="webImageInput" class="image-input" accept="image/*">
                                                                <input type="hidden" name="webImageURL" id="webImageURL" value="">
                                                                <span class="file-text">Seleccionar imagen</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <!-- Imagen de inicio móvil -->
                                                    <div class="col-md-6 col-12">
                                                        <span class="bg-warning rounded shadow-sm h5 px-2 my-1">Cambiar imagen de inicio móvil</span>
                                                        <div class="card my-4">
                                                            <label for="mobileImageInput" class="image-input-label">
                                                                <div class="image-container">
                                                                    <!-- Vista previa de la imagen actual -->
                                                                    <div id="mobileImagePreview">
                                                                        @if($setting->isEmpty() || is_null($setting[0]->image_background_movile))
                                                                            <span class="p-4">No hay una imagen cargada</span>
                                                                        @else
                                                                            <img src="{{ asset('storage/' . $setting[0]->image_background_movile) }}" class="image-preview" alt="Imagen de fondo">
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <!-- Input para seleccionar el archivo -->
                                                                <input hidden type="file" name="image_background_mobile" id="mobileImageInput" class="image-input" accept="image/*">
                                                                <input type="hidden" name="mobileImageURL" id="mobileImageURL" value="">
                                                                <span class="file-text">Seleccionar imagen</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    {{-- card title section --}}
                                                    <span class="mx-2 my-3 lead bg-info rounded p-2 col-md-12">
                                                        Esta sección se hizo con el fin de cambiar el contenido de texto de inicio:
                                                    </span>
                                                    {{-- btn input --}}
                                                    <div class="col-md-6 col-12">
                                                        <div class="bg-light rounded shadow-sm my-2">
                                                            <i class="fas fa-pencil-alt p-1"></i>
                                                            <span class="lead my-2">Ingrese el <em>título</em> de inicio</span>
                                                        </div>
                                                        {!! Form::text('title_background', null, ['class' => 'form-control']) !!}
                                                    </div>
                                                    {{-- btn input --}}
                                                    <div class="col-md-6 col-12">
                                                        <div class="bg-light rounded shadow-sm my-2">
                                                            <i class="fas fa-pencil-alt p-1"></i>
                                                            <span class="lead my-2">Ingrese una <em>descripción</em> de inicio</span>
                                                        </div>
                                                        {!! Form::textarea('description_background', null, ['class' => 'form-control', 'rows' => '3']) !!}
                                                    </div>
                                                </div>

                                                {{-- btn save --}}
                                                <div class="modal-footer">
                                                    {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                                                </div>

                                            {!! Form::close() !!}

                                            {{-- styles --}}
                                            <style>
                                                .image-input-label {
                                                    position: relative;
                                                    cursor: pointer;
                                                    display: inline-block;
                                                    overflow: hidden; /* Oculta el área del texto */
                                                }
                                            
                                                .image-container {
                                                    position: relative;
                                                    overflow: hidden;
                                                }
                                            
                                                .image-preview {
                                                    width: 100%;
                                                    height: 300px !important;
                                                    transition: opacity 0.3s ease-in-out;
                                                }
                                            
                                                .camera-icon {
                                                    position: absolute;
                                                    top: 50%;
                                                    left: 50%;
                                                    transform: translate(-50%, -50%);
                                                    font-size: 24px;
                                                    opacity: 0;
                                                    transition: opacity 0.3s ease-in-out;
                                                }
                                            
                                                .file-text {
                                                    position: absolute;
                                                    top: 0;
                                                    left: 0;
                                                    width: 100%;
                                                    height: 100%;
                                                    opacity: 0;
                                                    display: flex;
                                                    align-items: center;
                                                    justify-content: center;
                                                    color: transparent;
                                                    background-color: rgba(255, 255, 255, 0.8);
                                                    transition: opacity 0.3s ease-in-out;
                                                }
                                            
                                                .image-container:hover .image-preview {
                                                    opacity: 0.7;
                                                }
                                            
                                                .image-container:hover .camera-icon {
                                                    opacity: 1;
                                                }
                                            
                                                .image-input-label:hover .file-text {
                                                    opacity: 1;
                                                    color: #000; /* Cambiar color del texto cuando es visible */
                                                }
                                                .spacing-footer{
                                                    margin-top: 50px;
                                                }

                                                @media screen and (max-width: 768px) {
                                                    /* Estilos específicos para pantallas con un ancho máximo de 768 píxeles */
                                                    .image-preview {
                                                        width: 80%;
                                                        height: 200px !important;
                                                        transition: opacity 0.3s ease-in-out;
                                                    }
                                                    .spacing-footer{
                                                        margin-top: 150px;
                                                    }
                                                }
                                            </style>
                                            {{-- js changed image --}}
                                            <script>
                                                // Escucha cambios en el campo de entrada de la imagen web
                                                document.getElementById('webImageInput').addEventListener('change', function(event) {
                                                    handleImageUpload(event, 'webImagePreview', 'webImageURL');
                                                });

                                                // Escucha cambios en el campo de entrada de la imagen móvil
                                                document.getElementById('mobileImageInput').addEventListener('change', function(event) {
                                                    handleImageUpload(event, 'mobileImagePreview', 'mobileImageURL');
                                                });

                                                // Función para manejar la carga de imágenes
                                                function handleImageUpload(event, previewId, imageURLInputId) {
                                                    const input = event.target;
                                                    const preview = document.getElementById(previewId);
                                                    const imageURLInput = document.getElementById(imageURLInputId);

                                                    // Elimina la vista previa existente, si la hay
                                                    preview.innerHTML = '';

                                                    // Verifica si se seleccionó un archivo
                                                    if (input.files && input.files[0]) {
                                                        const reader = new FileReader();

                                                        reader.onload = function(e) {
                                                            // Crea un elemento de imagen y establece la vista previa
                                                            const img = document.createElement('img');
                                                            img.src = e.target.result;
                                                            img.classList.add('image-preview');
                                                            
                                                            // Agrega la imagen al contenedor de la vista previa
                                                            preview.appendChild(img);

                                                            // Almacena la ruta de la imagen en el campo oculto
                                                            imageURLInput.value = e.target.result;
                                                        };

                                                        // Lee el archivo como una URL de datos
                                                        reader.readAsDataURL(input.files[0]);
                                                    }
                                                }
                                                
                                            </script>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
        <footer class="main-footer spacing-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="">Mileer león</a>.</strong> All rights reserved.
        </footer>
    </div>
    
</div>

<!-- jQuery -->
<script src="../static/js/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- SweetAlert CDN -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<script>

$('#form_settings_change').submit(function (event) {
    event.preventDefault(); // Evita que el formulario se envíe de forma predeterminada

    // Serializa los datos del formulario utilizando FormData
    var formData = new FormData(this);

    // Envía el formulario de manera asíncrona utilizando AJAX
    $.ajax({
        url: $(this).attr('action'),
        method: $(this).attr('method'),
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            // Muestra una alerta después de guardar los datos
            Swal.fire({
                icon: 'success',
                title: '¡Datos guardados!',
                text: 'Los datos se han guardado correctamente.',
            }).then(function() {
                // Recarga la página después de cerrar la alerta
                location.reload();
            });
        },
        error: function (xhr, status, error) {

            // Muestra un mensaje de error genérico al usuario
            Swal.fire({
                icon: 'error',
                title: 'Error en la solicitud',
                text: 'Hubo un problema al intentar guardar los datos.',
            });
        }
    });
});
</script>
@extends('admin.master')

@include('admin.navBar')
{{-- table --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            {{-- message errors --}}
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- Content Header (Page header) -->
            <section class="content-header ">
                <div class="container-fluid">
                    <div class="row mb-2 mx-3">
                        <div class="">
                            <h1>Imagenes de publicidad</h1>
                        </div>
                        <div class="mx-4 mt-1">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                            <li class="breadcrumb-item active"><i class="fas fa-user-cog"></i> Imagenes de publicidad</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
        </div>
        <footer class="main-footer spacing-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="">Mileer león</a>.</strong> All rights reserved.
        </footer>
    </div>
    
</div>

<!-- jQuery -->
<script src="../static/js/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

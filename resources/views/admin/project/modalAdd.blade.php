<!-- Modal Add-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Proyecto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <!-- Tu formulario aquí -->
            {!! Form::open(['url' => '/admin/proyectos/add', 'files' => true, 'id' => 'project-form']) !!}
            @csrf
            <!-- ... (contenido del formulario) ... -->
                <div class="row my-2">
                    <div class="col">
                        <label for="name">Nombre del proyecto: </label>
                        <div>
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="name-error"></span>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="lastname">Descripción: </label>
                        <div>
                            {!! Form::textarea('description', null, ['class' => 'form-control', 'style' => 'height: 100px; max-height: 200px;']) !!}
                        </div>
                        <span class="text-danger" id="description-error"></span>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="price">Precio proyecto: </label>
                        <div>
                            {!! Form::number('price', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="price-error"></span>
                    </div>

                    <div class="col">
                        <label for="price_company">Presupuesto compañia: </label>
                        <div>
                            {!! Form::number('price_company', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="price_company-error"></span>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="price_material">Precio material: </label>
                        <div>
                            {!! Form::number('price_material', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="price_material-error"></span>
                    </div>

                    <div class="col">
                        <label for="employee_name">Empleado encargado: </label>
                        <div>
                            {!! Form::select('employee_name', $namiven, null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="employee_name-error"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="price_employee">Presupuesto empleado: </label>
                        <div>
                            {!! Form::number('price_employee', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="price_employee-error"></span>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                </div>

            {!! Form::close() !!}
            </div>

            <script>
                $(document).ready(function() {
                    $('#project-form').submit(function(event) {
                        event.preventDefault();
            
                        // Eliminar mensajes de error de todos los campos
                        $('.text-danger').empty();
            
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('project_add') }}',
                            data: $(this).serialize(),
                            success: function(response) {
                                if (response.errors) {
                                    // Manejar errores de validación
                                    $.each(response.errors, function(key, value) {
                                        $('#' + key + '-error').html(value[0]);
                                    });
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Debes completar el formulario!'
                                    })
                                } else if (response.success) {
                                    Swal.fire(
                                        '¡Buen trabajo!',
                                        'Guardado con éxito!',
                                        'success'
                                    )
                                    location.reload(); // Recargar la página
                                    // Puedes redirigir o realizar otras acciones después de guardar con éxito
                                }
                            }
                        });
                    });
            
                    // Eliminar mensajes de error cuando se enfoca en un campo
                    $('input, textarea, select').focusout(function() {
                        $(this).next('.text-danger').empty();
                    });
                });
            </script>
            
        </div>
    </div>
</div>
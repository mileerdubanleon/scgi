<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-dark">
        <div class="inner">
            <h3>{{ $numClientes }}</h3>
            <p class="lead">Clientes</p>
        </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">Más información <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

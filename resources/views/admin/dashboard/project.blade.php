<div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{ $numProject }}</h3>
            <p class="lead">Proyectos</p>
        </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">Más información <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

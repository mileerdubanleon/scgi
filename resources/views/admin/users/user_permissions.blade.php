@extends('admin.master')

@include('admin.navBar')

{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

{{-- table --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header px-3 pt-4 p-0">
                <div class="container-fluid card">
                    <div class="row mb-2">
                        <div class="mx-4 mt-1">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('/admin/usuarios') }}">Usuarios</a></li>
                            <li class="breadcrumb-item active"><i class="fas fa-cogs"></i> Permisos: {{ $u->name }}</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            
            @include('sweetalert::alert')

            <!-- Main content -->
            <section class="content card">
                <!-- Main content -->
                    <div class="container-fluid">
                        <form action="{{ url('/admin/usuarios/'.$u->id.'/permissions') }}" method="POST">
                            @csrf
                    
                            <div class="row mx-4 my-2">
                                @foreach(user_permissions() as $key => $value)
                                    <div class="col-md-4">
                                        <div class="panel p-4 shadow">
                                            <div class="">
                                                <h2 class="title">{!! $value['icon'] !!} {{ $value['title'] }}</h2>
                                            </div>
                                            <div class="inside">
                                                @foreach($value['keys'] as $k => $v)
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="{{ $k }}" type="checkbox" value="true" id="flexCheckDefault" @if(kvfj($u->permissions, $k)) checked @endif>
                                                        <label class="form-check-label" for="flexCheckDefault">{{ $v }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                    
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel my-4">
                                        <div class="inside">
                                        <input type="submit" value="Guardar" class="btn btn-primary">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
            <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="">Mileer león</a>.</strong> All rights reserved.
        </footer>

	</div>
	<!-- ./wrapper -->

</div>

<!-- jQuery -->
		<script src="{{ asset('static/js/plugins/jquery/jquery.min.js') }}"></script>
		<!-- jQuery UI 1.11.4 -->
		<script src="{{ asset('static/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		





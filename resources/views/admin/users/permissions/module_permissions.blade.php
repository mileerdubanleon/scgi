<div class="col-md-4">
    <div class="card ">
        <div class="header my-3 px-4">
            <h5><i class="nav-icon fas fa-inbox"></i> Modulo de roles y permisos</h5>
        </div>

        <div class="px-5">
            <div class="container d-flex">
                <input style="width: 25px; height: 25px;" type="checkbox" value="true" name="permission_module" @if(kvfj($u->permissions, 'permission_module')) checked @endif>
                <label for="permission_module" class="mx-3"> Puede ver modulo de permisos</label>
            </div>
        </div>

        <div class="px-5">
            <div class="container d-flex">
                <input style="width: 25px; height: 25px;" type="checkbox" value="true" name="users" @if(kvfj($u->permissions, 'users')) checked @endif>
                <label for="users" class="mx-3"> Puede ver los usuarios</label>
            </div>
        </div>

        <div class="px-5">
            <div class="container d-flex">
                <input style="width: 25px; height: 25px;" type="checkbox" value="true" name="user_permissions" @if(kvfj($u->permissions, 'user_permissions')) checked @endif>
                <label for="user_permissions" class="mx-3"> Puede ver los permisos</label>
            </div>
        </div>

        <div class="px-5 mb-3">
            <div class="container d-flex">
                <input style="width: 25px; height: 25px;" type="checkbox" value="true" name="user_permissions_add" @if(kvfj($u->permissions, 'user_permissions_add')) checked @endif>
                <label for="user_permissions_add" class="mx-3"> Puede actualizar los permisos</label>
            </div>
        </div>
    </div>
</div>
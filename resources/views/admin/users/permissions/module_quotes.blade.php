<div class="col-md-4">
    <div class="card ">
        <div class="header my-3 px-4">
            <h5><i class="nav-icon fas fa-wallet"></i> Modulo de cotizaciones</h5>
        </div>

        <div class="px-5">
            <div class="container d-flex">
                <input style="width: 25px; height: 25px;" type="checkbox" value="true" name="quotes" @if(kvfj($u->permissions, 'quotes')) checked @endif>
                <label for="quotes" class="mx-3"> Puede ver las cotizaciones</label>
            </div>
        </div>

        <div class="px-5 mb-3">
            <div class="container d-flex">
                <input style="width: 25px; height: 25px;"  type="checkbox" value="true" name="quotes_add" @if(kvfj($u->permissions, 'quotes_add')) checked @endif>
                <label for="quotes_add" class="mx-3"> Puede crear una cotización</label>
            </div>
        </div>
    </div>
</div>
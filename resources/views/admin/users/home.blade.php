@extends('admin.master')

@include('admin.navBar')

<!-- DataTables -->
<link rel="stylesheet" href="../static/js/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../static/css/adminlte.min.css">

{{-- table --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mx-3">
                    <div class="">
                        <h1>Usuario</h1>
                    </div>
                    <div class="mx-4 mt-1">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                        <li class="breadcrumb-item active"><i class="fas fa-user-cog"></i> Usuarios</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="font-weight-bold">
                                                <td>Nombres</td>
                                                <td>Apellidos</td>
                                                <td>Email</td>
                                                <td>Role</td>
                                                <td>Estado</td>
                                                <td>Acción</td>
                                            </tr>
                                        </thead>
                                    <tbody>
                                        @foreach($user as $u)
                                            <tr>
                                                <td>{{ $u->name }}</td>
                                                <td>{{ $u->lastname}}</td>
                                                <td>{{ $u->email }}</td>
                                                <td>{{ getRoleUserArray(null,$u->role) }}</td>
						                        <td>{{ getUserStatusArray(null,$u->status) }}</td>
                                                <td width="160">
                                                    <div class="opts">
                                                        <a href="{{ url('/admin/users/'.$u->id.'/edit') }}" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-success mx-2">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        @if(kvfj(Auth::user()->permissions, 'user_permissions_add'))
                                                            <a href="{{ url('/admin/usuarios/'.$u->id.'/permissions') }}" data-toggle="tooltip" data-placement="top" title="Permisos" class="btn btn-info mx-2">
                                                                <i class="fas fa-cogs"></i>
                                                            </a>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                        <tfoot>
                                            <tr class="font-weight-bold">
                                                <td>Nombres</td>
                                                <td>Apellidos</td>
                                                <td>Email</td>
                                                <td>Role</td>
                                                <td>Estado</td>
                                                <td>Acción</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="https://adminlte.io">Mileer león</a>.</strong> All rights reserved.
        </footer>

	</div>
	<!-- ./wrapper -->

	<!-- jQuery -->
	<script src="../static/js/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script>
	$(function () {
		$("#example1").DataTable({
		"responsive": true, "lengthChange": false, "autoWidth": false,
		"buttons": ["excel", "pdf", "print", "colvis"]
		}).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
		$('#example2').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"responsive": true,
		});
	});
	</script>
</div>





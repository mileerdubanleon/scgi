@extends('admin.master')

@include('admin.navBar')

<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- DataTables CSS CDN -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
{{-- table --}}
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
				<div class="container-fluid">
					<div class="row mb-2 mx-3">
						<div class="">
							<h1>Usuario</h1>
						</div>
						<div class="mx-4 mt-1">
							<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
							<li class="breadcrumb-item "><a href="{{ url('/admin/usuarios') }}">Usuarios</a> </li>
							<li class="breadcrumb-item active"><i class="fas fa-user-cog"></i> id:{{ $u->id }} - Editar Usuario {{ $u->name }}</li>
							</ol>
						</div>
					</div>
				</div><!-- /.container-fluid -->
            </section>

			<!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
								<div class="p-3 row">

									<div class="col-md-6 mb-5 p-3">
										<div class="p-3">
											<div class="">
												<h2 class="title">Información Usuario</h2>
											</div>
							
											<div class="container">
												
					
												<div class="d-inline-block">
													<div>
														@if(is_null($u->avatar))
															<img src="{{ url('/static/images/default-avatar.png') }}" class="" style="width: 150px; height: 150px;border: 2px solid #27aef7; border-radius:50%;">
														@else
															<img src="{{ url('/uploads_user/'.$u->id.'/'.$u->avatar) }}" class="my-3 shadow" style="width: 150px; height: 150px;border: 2px solid #27aef7;border-radius:50%;" alt="Avatar">
														@endif
													</div>
													<div class="d-flex">
														<span class="font-weight-bold mx-2"> Nombre:</span>
														<span class="text">{{ $u->name }} {{ $u->lastname }} </span>
													</div>
													<div class="d-flex">
														<span class="font-weight-bold mx-2"> Estado del usuario:</span>
														<span class="text">{{ getUserStatusArray(null,$u->status) }}</span>
													</div>
													<div class="d-flex">
														<span class="font-weight-bold mx-2">Correo electrónico:</span>
														<span class="text">{{ $u->email }}</span>
													</div>
													<div class="d-flex">
														<span class="font-weight-bold mx-2"><i class="fas fa-calendar-alt"></i> Fecha de registro:</span>
														<span class="text">{{ $u->created_at }}</span>
													</div>
													<div class="d-flex">
														<span class="font-weight-bold mx-2"><i class="fas fa-user-shield"></i> Role de usuario:</span>
														<span class="text">{{ getRoleUserArray(null,$u->role) }}</span>
													</div>
													
												</div>
												<div class="my-3 mx-3">
													@if(kvfj(Auth::user()->permissions, 'user_banned'))
														@if($u->status  == "100")
															<a href="{{ url('/admin/user/'.$u->id.'/banned') }}" class="btn btn-success">Activar Usuario</a>
														@else
															<a href="{{ url('/admin/user/'.$u->id.'/banned') }}" class="btn btn-danger">Suspender Usuario</a>
														@endif
													@endif
												</div>
											</div>
										</div>
									</div>
							
									<div class="col-md-6 p-3">
										<div class="">
											<div class="d-flex justify-content-center">
												<h2> Editar Información</h2>
											</div>
					
												{!! Form::open(['url' => '/admin/usuario/'.$u->id.'/edit']) !!}
													<div class="row">
									
														<div class="col-md-12 container">
															<label class="my-3">Tipo de usuario:</label>
															<span class="" id="basic-addon1"></span>
															{!! Form::select('user_type', getRoleUserArray('list', null), $u->role, ['class' => 'form-control']) !!}
														</div>
									
													</div>
							
												<div class="row my-3">
													<div class="col-md-12">
														{!!Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
													</div>
												</div>
												{!! Form::close() !!}
					
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	</div>
</div>
<!-- jQuery y Bootstrap JS CDN -->
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

{{-- dataTables archivo de dependencias --}}
@include('admin.components.datatable')
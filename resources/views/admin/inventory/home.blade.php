@extends('admin.master')

@include('admin.navBar')

<!-- DataTables -->
<link rel="stylesheet" href="../static/js/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2 mx-3">
                        <div class="">
                            <h1>Inventario</h1>
                        </div>
                        <div class="mx-4 mt-1">
                            <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
                            <li class="breadcrumb-item active">Inventario ferretería</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-12">
                            <div class="card">
                                @if(kvfj(Auth::user()->permissions, 'inventario_btn'))
                                    <div class="card-header">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                            + Agregar Producto Ferretería
                                        </button>
                                    </div>
                                @endif
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="font-weight-bold">
                                                <td class="border-0">ID</td>
                                                <td class="border-0">Nombre producto</td>
                                                <td class="border-0">Codigo</td>
                                                <td class="border-0">Precio proveedor</td>
                                                <td class="border-0">Precio venta</td>
                                                <td class="border-0">Estado</td>
                                                <td class="border-0">Acción</td>
                                            </tr>
                                        </thead>
                                    <tbody>
                                        @foreach($inventory as $i)
                                            <tr>
                                                <td>{{ $i->id }}</td>
                                                <td>{{ $i->name }}</td>
                                                <td>{{ $i->codigo}}</td>
                                                <td>{{ $i->price }}</td>
                                                <td>{{ $i->price_sale }}</td>
                                                <td>
                                                    <button class="btn btn-toggle-state {{ $i->state == 0 ? 'btn-danger' : 'btn-success' }}" data-item-id="{{ $i->id }}" data-current-state="{{ $i->state }}">
                                                        @if($i->state == 1)
                                                            <span class="btn-text">Activo</span>
                                                        @else
                                                            <span class="btn-text">Inactivo</span>
                                                        @endif
                                                    </button>
                                                </td>
                                                <td>
                                                    @if(kvfj(Auth::user()->permissions, 'inventario_edit'))
                                                        <a href="{{ url('/admin/inventario/'.$i->id.'/edit') }}" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-success mx-2">
                                                            Editar
                                                        </a>
                                                    @endif
                                                    @if(kvfj(Auth::user()->permissions, 'inventario_delete'))
                                                        <a href="{{ url('/admin/inventario/'.$i->id.'/delete') }}" onclick="confirmDelete('{{ $i->id }}', event)" data-path="admin/inventory" data-action="delete" data-object="{{ $i->id }}" data-toggle="tooltip" data-placement="top" title="Eliminar" class="btn btn-danger">
                                                            Eliminar
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                        <tfoot>
                                            <tr class="font-weight-bold">
                                                <td>ID</td>
                                                <td>Nombre producto</td>
                                                <td>Codigo</td>
                                                <td>Precio proveedor</td>
                                                <td>Precio venta</td>
                                                <td>Estado</td>
                                                <td>Acción</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="">Mileer león</a>.</strong> All rights reserved.
        </footer>

	</div>
	<!-- ./wrapper -->

	<!-- jQuery -->
	<script src="../static/js/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ./wrapper -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    {{-- axios for btn state --}}
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    {{-- escript with functions --}}
	<script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true, "lengthChange": false, "autoWidth": false,
            "buttons": ["excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            });
        });

        // sweet alert
        function confirmDelete(id, event) {
            event.preventDefault();  // Evitar el comportamiento predeterminado del enlace
            
            Swal.fire({
                title: '¿Estás seguro?',
                text: 'Esta acción no se puede deshacer',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Sí, eliminar'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = '{{ url("/admin/inventario") }}/' + id + '/delete';
                }
            });
        }

        // btn state
        document.addEventListener('DOMContentLoaded', function () {
            const buttons = document.querySelectorAll('.btn-toggle-state');

            buttons.forEach(button => {
                button.addEventListener('click', function () {
                    const itemId = this.dataset.itemId;
                    const currentState = this.dataset.currentState;

                    // Realiza una solicitud al servidor para cambiar el estado
                    axios.post('/admin/cambiarEstado', { itemId, currentState })
                        .then(response => {
                            // Actualiza la interfaz con el nuevo estado devuelto por el servidor
                            const newState = response.data.newState;
                            this.dataset.currentState = newState;

                            // Actualiza el texto en el botón
                            const btnText = this.querySelector('.btn-text');
                            btnText.textContent = newState === '1' ? 'Activo' : 'Inactivo';

                            // Actualiza las clases del botón según el nuevo estado
                            this.classList.remove(newState === '1' ? 'btn-danger' : 'btn-success');
                            this.classList.add(newState === '1' ? 'btn-success' : 'btn-danger');
                        })
                        .catch(error => {
                            console.error('Error al cambiar el estado:', error);
                        });
                });
            });
        });
	</script>
</div>

@include('admin.inventory.modalAdd')





<!-- Modal Add-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar producto ferretería</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <!-- Tu formulario aquí -->
            {!! Form::open(['url' => '/admin/inventario/add', 'files' => true, 'id' => 'inventory-form']) !!}
            @csrf
            <!-- ... (contenido del formulario) ... -->
                <div class="row my-2">
                    <div class="col">
                        <label for="name">Nombre del producto: </label>
                        <div>
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <span class="text-danger" id="name-error"></span>
                    </div>

                    <div class="col">
                        <label for="codigo">Código: </label>
                        <div>
                            {!! Form::text('codigo', null, ['class' => 'form-control', 'id' => 'codigo-input']) !!}
                        </div>
                        <span class="text-danger" id="codigo-error"></span>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="price">Precio: </label>
                        <div>
                            {!! Form::number('price', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="price-error"></span>
                    </div>
                    <div class="col">
                        <label for="price_sale">Precio de venta: </label>
                        <div>
                            {!! Form::number('price_sale', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                        <span class="text-danger" id="price_sale-error"></span>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-success', 'id' => 'guardar-btn', 'disabled']) !!}
                </div>

            {!! Form::close() !!}
            </div>

            <script>
                $(document).ready(function() {
                    // Verificar el codigo en tiempo real
                    $('#codigo-input').on('input', function() {
                        var codigo = $(this).val();

                        // Eliminar mensajes de error y habilitar el botón
                        $('#codigo-error').empty();
                        $('#guardar-btn').prop('disabled', false);

                        // Verificar el codigo mediante AJAX
                        $.ajax({
                            type: 'GET',
                            url: '{{ route('inventario') }}',
                            data: { codigo: codigo },
                            success: function(response) {
                                if (response.exists) {
                                    $('#codigo-error').html('El codigo ya existe.');
                                    $('#guardar-btn').prop('disabled', true);
                                }
                            }
                        });
                    });
                    
                    // enviar el formulario
                    $('#inventory-form').submit(function(event) {
                        event.preventDefault();
            
                        // Eliminar mensajes de error de todos los campos
                        $('.text-danger').empty();
            
                        $.ajax({
                            type: 'POST',
                            url: '{{ route('inventario_add') }}',
                            data: $(this).serialize(),
                            success: function(response) {
                                if (response.errors) {
                                    // Manejar errores de validación
                                    $.each(response.errors, function(key, value) {
                                        $('#' + key + '-error').html(value[0]);
                                    });
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: 'Debes completar el formulario!'
                                    })
                                } else if (response.success) {
                                    Swal.fire(
                                        '¡Buen trabajo!',
                                        'Guardado con éxito!',
                                        'success'
                                    )
                                    location.reload(); // Recargar la página
                                    // Puedes redirigir o realizar otras acciones después de guardar con éxito
                                }
                            }
                        });
                    });
            
                    // Eliminar mensajes de error cuando se enfoca en un campo
                    $('input, textarea, select').focusout(function() {
                        $(this).next('.text-danger').empty();
                    });
                });
            </script>
        </div>
    </div>
</div>
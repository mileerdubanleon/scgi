@extends('admin.master')

@include('admin.navBar')

  <!-- DataTables -->
  <link rel="stylesheet" href="../static/js/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../static/js/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="../static/js/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../static/css/adminlte.min.css">
</head>
<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mx-3">
                    <div class="">
                        <h1>Inventario</h1>
                    </div>
                    <div class="mx-4 mt-1">
                        <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Home</a></li>
                        <li class="breadcrumb-item active">Estadísticas / Inventario</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
            </section>

            {{-- Estadisticas de totales --}}
            <div class="d-flex mx-4">
                {{-- Total de precios unitarios --}}
                <div class="my-3 mx-3 bg-white" style="border: 1px solid #000; padding: 10px;">
                    <span class="font-weight-bold">Total Precio unitario:</span>
                    <div class="mx-2 h2">
                        B/. {{ number_format($totalPrecioUnitario, 2) }} {{-- Esto mostrará el total con dos decimales --}}
                    </div>
                </div>
                {{-- Total de precios ventas --}}
                <div class="my-3 mx-3 bg-white" style="border: 1px solid #000; padding: 10px;">
                    <span class="font-weight-bold">Total Precio venta:</span>
                    <div class="h2">
                        B/. {{ number_format($totalPrecioVenta, 2) }} {{-- Esto mostrará el total con dos decimales --}}
                    </div>
                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="font-weight-bold">
                                                <td>Codigo</td>
                                                <td>Nombre producto</td>
                                                <td>Precio compra und.</td>
                                                <td>Precio venta und.</td>
                                                <td>Stock</td>
                                                <td>Fecha de salida</td>
                                                <td>Precio total compra B/.</td>
                                                <td>Precio total venta B/.</td>
                                            </tr>
                                        </thead>
                                    <tbody>
                                        @foreach($inventory as $i)
                                            <tr>
                                                <td>{{ $i->codigo}}</td>
                                                <td>{{ $i->name }}</td>
                                                <td>{{ $i->price }}</td>
                                                <td>{{ $i->price_sale }}</td>
                                                <td style="background-color: {{ $i->available_quantity <= 10 ? 'red' : 'green' }}; color: white;">
                                                    {{ $i->available_quantity }}
                                                </td>
                                                <td>
                                                    @foreach($i->output as $out)
                                                        {{ $out->created_at }}
                                                    @endforeach
                                                </td>
                                                <td><span></span>{{ number_format($i->price * $i->available_quantity, 2, '.', '') }}</td>
                                                <td><span></span>{{ number_format($i->price_sale * $i->available_quantity, 2, '.', '') }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                        <tfoot>
                                            <tr class="font-weight-bold">
                                                <td>Codigo</td>
                                                <td>Nombre producto</td>
                                                <td>Precio compra</td>
                                                <td>Precio venta</td>
                                                <td>Stock</td>
                                                <td>Fecha de salida</td>
                                                <td>Precio total compra</td>
                                                <td>Precio total venta</td>
                                            </tr>
                                        </tfoot>
                                        
                                    </table>

                                </div>
                            <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0
            </div>
            <strong>Copyright &copy; 2023 <a href="https://adminlte.io">Mileer león</a>.</strong> All rights reserved.
        </footer>

	</div>
	<!-- ./wrapper -->

	<!-- jQuery -->
	<script src="../static/js/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

	<script>
	$(function () {
		$("#example1").DataTable({
		"responsive": true, "lengthChange": false, "autoWidth": false,
		"buttons": ["excel", "pdf", "print", "colvis"]
		}).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
		$('#example2').DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": false,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"responsive": true,
		});
	});
	</script>
</div>

<!-- Modal Add-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar inventario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <!-- Tu formulario aquí -->
            {!! Form::open(['url' => '/admin/inventario/add', 'files' => true]) !!}
            <!-- ... (contenido del formulario) ... -->
                <div class="row my-2">
                    <div class="col">
                        <label for="name">Nombre del producto: </label>
                        <div>
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col">
                        <label for="codigo">Código: </label>
                        <div>
                            {!! Form::text('codigo', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>

                <div class="row my-2">
                    <div class="col">
                        <label for="price">Precio: </label>
                        <div>
                            {!! Form::number('price', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                    </div>
                    <div class="col">
                        <label for="price_sale">Precio de venta: </label>
                        <div>
                            {!! Form::number('price_sale', null, ['class' => 'form-control', 'step' => 'any']) !!}
                        </div>
                    </div>
                </div>

                <div class="row my-4">
                    <div class="col-md-6">
                        <label for="state">Estado: </label>
                        <div>
                            {!! Form::select('state', ['1' => 'ACTIVO', '0' => 'INACTIVO'], null, ['class' => 'form-select form-control']) !!}
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                </div>

            {!! Form::close() !!}
            </div>

            
        </div>
    </div>
</div>




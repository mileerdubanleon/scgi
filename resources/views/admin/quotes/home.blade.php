@extends('admin.master')

@include('admin.navBar')

<!-- DataTables -->
<link rel="stylesheet" href="../static/js/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../static/js/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../static/css/adminlte.min.css">

<div class="hold-transition sidebar-mini">
	<div class="wrapper">

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header px-4">
                <div class="bg-white mx-2 rounded">
                    <div class="px-4 py-4">
                        <span class="h4">+ Agregando cliente</span>
                    </div>

                    <!-- Content quotes -->
                    <div class="px-5 shadow-sm">
                        <!-- Tu formulario aquí -->
                        {!! Form::open(['url' => '/admin/quotes/add', 'files' => true, 'id' => 'quotes-form']) !!}
                        @csrf
                        <!-- ... (contenido del formulario) ... -->
                            <div class="">
                                <div class="row my-4">
                                    <div class="mx-auto">
                                        <label for="client_name">Nombre del cliente: </label>
                                        <div>
                                            {!! Form::text('client_name', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <span class="text-danger" id="client_name-error"></span>
                                    </div>
    
                                    <div class="mx-auto">
                                        <label for="address">Dirección: </label>
                                        <div>
                                            {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <span class="text-danger" id="address-error"></span>
                                    </div>

                                    <div class="mx-auto">
                                        <label for="email">Correo: </label>
                                        <div>
                                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <span class="text-danger" id="email-error"></span>
                                    </div>
        
                                    <div class="mx-auto">
                                        <label for="phone">Telefono: </label>
                                        <div>
                                            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <span class="text-danger" id="phone-error"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="px-5">
                    <button id="loadDetailsButton" class="btn btn-secondary btn-lg btn-block my-3">Ver detalles de la cotización </button>
                </div>

                <div id="detailsContainer"></div>
            </section>

        </div>

	</div>
	<!-- ./wrapper -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<!-- jQuery -->
	<script src="../static/js/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="../static/js/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


</div>

{{-- add client --}}
<script>
    $(document).ready(function() {
        $('#quotes-form').submit(function(event) {
            event.preventDefault();

            // Eliminar mensajes de error de todos los campos
            $('.text-danger').empty();

            $.ajax({
                type: 'POST',
                url: '{{ route('quotes_add') }}',
                data: $(this).serialize(),
                success: function(response) {
                    if (response.errors) {
                        // Manejar errores de validación
                        $.each(response.errors, function(key, value) {
                            $('#' + key + '-error').html(value[0]);
                        });
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Debes completar el formulario!'
                        })
                    } else if (response.success) {
                        Swal.fire(
                            '¡Buen trabajo!',
                            'Guardado con éxito!',
                            'success'
                        )
                        location.reload(); // Recargar la página
                        // Puedes redirigir o realizar otras acciones después de guardar con éxito
                    }
                }
            });
        });

        // Eliminar mensajes de error cuando se enfoca en un campo
        $('input, textarea, select').focusout(function() {
            $(this).next('.text-danger').empty();
        });
    });
</script>

{{-- view details quotes --}}
<script>
    $(document).ready(function() {
        // Manejar el clic en el botón
        $('#loadDetailsButton').click(function() {
            // Realizar una solicitud AJAX para cargar los detalles de la cotización
            $.ajax({
                url: "{{ route('details_quotes') }}", // URL de la ruta de detalles de cotización
                method: "GET",
                success: function(data) {
                    // Insertar los detalles de la cotización en el contenedor
                    $('#detailsContainer').html(data);
                },
                error: function() {
                    alert("Error al cargar los detalles de la cotización.");
                }
            });
        });
    });
</script>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Alumart</title>

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="routeName" content="{{ Route::currentRouteName() }}">

	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/fontawesome-free/css/all.min.css') }}">
	<!-- Tempusdominus Bootstrap 4 -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
	{{-- icons bootstrap --}}

	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('static/css/adminlte.min.css') }}">
	<!-- css style -->
	<link rel="stylesheet" href="{{ asset('static/css/content.css') }}">
	<link rel="stylesheet" href="{{ asset('static/css/app.css') }}">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/daterangepicker/daterangepicker.css') }}">
	<!-- summernote -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/summernote/summernote-bs4.min.css') }}">
	
</head>

<body class="hold-transition sidebar-mini layout-fixed ">

		<!-- Preloader -->
		{{-- <div class="preloader flex-column justify-content-center align-items-center">
			<img class="animation__shake" src="{{ asset('../static/images/loading.gif') }}" alt="logo" height="60" width="60">
		</div> --}}

		<!-- Preloader -->
		<div class="spinner-wrapper">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
		<!-- end of preloader -->

		<!-- Main Sidebar Container -->
		<aside class="main-sidebar sidebar-dark-bg elevation-2">
			<div>
				<!-- Brand Logo -->
				<a href="{{ url('/admin') }}" class="mx-4">
					<img src="{{ asset('static/images/logo.png') }}" alt="Logo" class=" my-2" style="opacity: .8; width: 40px; height: 60px;">
				</a>
			</div>

			<!-- Sidebar -->
			<div class="sidebar">
				<!-- Sidebar user panel (optional) -->
				<div class="">
					<div class="row justify-content-center mx-1">
						@if(Auth::user()->avatar)
                        	<img src="{{ asset('/uploads_user/' . Auth::id() . '/' . Auth::user()->avatar) }}" class="img-circle elevation-2" style="width: 100px; height: 100px;" alt="Avatar">
							
							<div class="row">
								<div class="col-md-12 my-3 mx-2">
									<span class="lead">{{ Auth::user()->name }}</span>
									@if(Auth::user()->role == 1)
										<div class="">
											<span class="">Administrador</span>
										</div>
									@else
										<div class="">
											<span class="">Empleado</span>
										</div>
									@endif
									<span style="font-size: 10px;">{{ Auth::user()->email }}</span>
								</div>
							</div>
						@else
							<img src="{{ asset('/static/images/default-avatar.png') }}" alt="Default Avatar">
						@endif
					</div>
					<div class="info">
						<a href="{{ url('/logout') }}" class="text-dark bg-red d-flex justify-content-center btn btn-outline-light mt-3">Cerrar sesión</a>
					</div>
				</div>

				<!-- Sidebar Menu -->
				<nav class="mt-2 pb-5">
					
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
					<!-- Add icons to the links using the .nav-icon class
						with font-awesome or any other icon font library -->
						<li class="nav-item menu-open">
							{{-- Dashboard --}}
							@if(kvfj(Auth::user()->permissions, 'dashboard'))
								<li class="nav-item mt-2">
									<a href="{{ url('/admin') }}" class="nav-link p-1 lk-dashboard">
										<i class="nav-icon fas fa-home"></i>
									<p class="text-sm text-dark">INICIO </p>
									</a>
								</li>
							@endif
							{{-- Dashboard --}}

							{{-- Clientes --}}
							@if(kvfj(Auth::user()->permissions, 'clients'))
								<div class="text-sm mx-1 my-2 font-weight bg-danger rounded">
									<span class="p-1 mx-1">COTIZACIONES</span>
								</div>
								<li class="nav-item">
									<a href="{{ url('/admin/clientes') }}" class="nav-link p-1 lk-clients">
										<i class="nav-icon fas fa-users"></i>
									<p class="text-sm text-dark">CLIENTES </p>
									</a>
								</li>
							@endif
							{{-- Clientes --}}

							{{-- item --}}
							@if(kvfj(Auth::user()->permissions, 'items'))
								<li class="nav-item">
									<a href="{{ url('/admin/items') }}" class="nav-link p-1 lk-items">
										<i class="nav-icon fas fa-wallet"></i>
									<p class="text-sm text-dark">CREAR COTIZACIONES </p>
									</a>
								</li>
							@endif
							{{-- item --}}
							{{-- quotations --}}
							@if(kvfj(Auth::user()->permissions, 'quotationsConsult'))
								<li class="nav-item">
									<a href="{{ url('/admin/quotationsConsult') }}" class="nav-link p-1 lk-items">
										<i class="nav-icon fas fa-search"></i>
									<p class="text-sm text-dark">CONSULTAR COTIZACIONES </p>
									</a>
								</li>
							@endif
							{{-- quotations --}}

							<div class="border-bottom mb-1 mt-1"></div>

							{{-- products ferreteria --}}
							@if(kvfj(Auth::user()->permissions, 'inventario'))
								<div class="text-sm mx-1 my-2 font-weight bg-warning rounded">
									<span class="p-1 mx-1">INVENTARIO FERRETERÍA</span>
								</div>
								<li class="nav-item ">
									<a href="{{ url('/admin/inventario') }}" class="nav-link p-1 lk-inventario">
										<i class="nav-icon fas fa-boxes"></i>
										<p class="text-sm">
											FERRETERÍA
										</p>
									</a>
								</li>
							@endif
							{{-- product end --}}

							{{-- inputs ferreteria --}}
							@if(kvfj(Auth::user()->permissions, 'input'))
								<li class="nav-item ">
									<a href="{{ url('/admin/entradas') }}" class="nav-link p-1 lk-entradas">
										<i class="nav-icon fas fa-box"></i>
										<p class="text-sm">
											ENTRADAS
										</p>
									</a>
								</li>
							@endif
							{{-- inputs end --}}

							{{-- output ferreteria --}}
							@if(kvfj(Auth::user()->permissions, 'output'))
								<li class="nav-item ">
									<a href="{{ url('/admin/salidas') }}" class="nav-link p-1 lk-salidas">
										<i class="nav-icon fas fa-hand-holding"></i>
										<p class="text-sm">
											SALIDAS
										</p>
									</a>
								</li>
							@endif
							{{-- output end --}}

							{{-- stadistica ferreteria --}}
							@if(kvfj(Auth::user()->permissions, 'stadisticas'))
								<li class="nav-item">
									<a href="{{ url('/admin/estadisticas') }}" class="nav-link p-1 lk-stadisticas">
										<i class="nav-icon fas fa-chart-pie"></i>
										<p class="text-sm">
											ESTADíSTICA
										</p>
									</a>
								</li>
							@endif
							{{-- stadistica end --}}

							<div class="border-bottom mb-1 mt-1"></div>

							{{-- employee --}}
							@if(kvfj(Auth::user()->permissions, 'employee'))
								<div class="text-sm mx-1 my-2 font-weight bg-success rounded">
									<span class="p-1 mx-1">OPERATIVO</span>
								</div>
								<li class="nav-item">
									<a href="{{ url('/admin/empleados') }}" class="nav-link px-1 lk-employee">
										<i class="nav-icon fas fa-people-carry"></i>
										<p class="text-sm">
											EMPLEADOS
										</p>
									</a>
								</li>
							@endif
							{{-- end employee --}}

							{{-- projects --}}
							@if(kvfj(Auth::user()->permissions, 'project'))
								<li class="nav-item">
									<a href="{{ url('/admin/proyectos') }}" class="nav-link p-1 lk-project">
										<i class="nav-icon fas fa-truck"></i>
										<p class="text-sm">
											PROYECTOS
										</p>
									</a>
								</li>
							@endif
							{{-- end projects --}}

							<div class="border-bottom mb-1 mt-1"></div>

							{{-- settings --}}
							@if(kvfj(Auth::user()->permissions, 'settings'))
								<div class="text-sm mx-1 my-2 font-weight bg-info rounded">
									<span class="p-1 mx-1">AJUSTES PAGINA PRINCIPAL</span>
								</div>
								<li class="nav-item">
									<a href="{{ url('/admin/settings') }}" class="nav-link px-1 lk-settings">
										<i class="nav-icon fas fa-vector-square"></i>
										<p class="text-sm">
										IMAGENES DE INICIO
										</p>
									</a>
								</li>
							@endif
							{{-- end settings --}}

							{{-- settings --}}
							@if(kvfj(Auth::user()->permissions, 'publicity'))
								<li class="nav-item">
									<a href="{{ url('/admin/publicity') }}" class="nav-link px-1 lk-publicity">
										<i class="nav-icon fas fa-ad"></i>
										<p class="text-sm">
										CARRUSEL DE PUBLICIDAD
										</p>
									</a>
								</li>
							@endif
							{{-- end settings --}}

							<div class="border-bottom mb-1 mt-1"></div>
							
							{{-- Inventory --}}
							@if(kvfj(Auth::user()->permissions, 'quotes'))
								{{-- quotes --}}
								<li class="nav-item">
									<a href="#" class="nav-link border-buttom">
										<p class="text-sm text-dark">
											<i class="nav-icon fas fa-wallet"></i>
											COTIZACIONES
											<i class="right fas fa-angle-left"></i>
										</p>
									</a>
									<ul class="nav nav-treeview">
										@if(kvfj(Auth::user()->permissions, 'quotes_add'))
											<li class="">
												<a href="{{ url('/admin/cotizaciones') }}" class="nav-link lk-quotes px-5">
													<i class="nav-icon fas fa-file"></i>
													<p class="text-sm">
														Crear cotización
													</p>
												</a>
											</li>
										@endif
										@if(kvfj(Auth::user()->permissions, 'quotes'))
											<li class="my-1">
												<a href="{{ url('/admin/tableCotizaciones') }}" class="nav-link lk-table_quotes px-5 lk-table_quotes_cli">
													<i class="nav-icon fas fa-table"></i>
													<p class="text-sm">
														Cotizaciones
													</p>
												</a>
											</li>
										@endif
									</ul>
								</li>
								{{-- end quotes --}}
							@endif
							{{-- Inventory --}}

							{{-- permissions --}}
							@if(kvfj(Auth::user()->permissions, 'permission_module'))
								<li class="nav-item">
									<a href="#" class="nav-link border-buttom">
										<p class="text-sm text-dark">
											<i class="nav-icon fas fa-user"></i>
												ROLES Y PERMISOS
											<i class="right fas fa-angle-left"></i>
										</p>
									</a>
									<ul class="nav nav-treeview">
										@if(kvfj(Auth::user()->permissions, 'users'))
											<li class="">
												<a href="{{ url('/admin/usuarios') }}" class="nav-link lk-users lk-user_permissions px-5">
													<i class="fas fa-user-cog"></i>
													<p class="text-sm">
														Usuarios
													</p>
												</a>
											</li>
										@endif
									</ul>
								</li>
							@endif
							{{-- end permissions --}}
						</li>
					</ul>
				</nav>
				<!-- /.sidebar-menu -->
			</div>
			<!-- /.sidebar -->
		</aside>

		<!-- ./wrapper -->

		{{-- <!-- jQuery -->
		<script src="{{ asset('static/js/plugins/jquery/jquery.min.js') }}"></script>
		<!-- jQuery UI 1.11.4 -->
		<script src="{{ asset('static/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> --}}
		
		
		<!-- Bootstrap 4 -->
		<script src="{{ asset('static/js/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
		<!-- ChartJS -->
		<script src="{{ asset('static/js/plugins/chart.js/Chart.min.js') }}"></script>

		<!-- DataTables & Plugins -->
		<script src="{{ asset('static/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/jszip/jszip.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/pdfmake/pdfmake.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/pdfmake/vfs_fonts.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

		<!-- jQuery Knob Chart -->
		<script src="{{ asset('static/js/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
		<!-- daterangepicker -->
		<script src="{{ asset('static/js/plugins/moment/moment.min.js') }}"></script>
		<script src="{{ asset('static/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
		<!-- Tempusdominus Bootstrap 4 -->
		<script src="{{ asset('static/js/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
		<!-- Summernote -->
		<script src="{{ asset('static/js/plugins/summernote/summernote-bs4.min.js') }}"></script>
		<!-- overlayScrollbars -->
		<script src="{{ asset('static/js/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
		<!-- AdminLTE App -->
		<script src="{{ asset('static/js/adminlte.js') }}"></script>
		{{-- js admin --}}
		<script src="{{ asset('static/js/admin.js') }}"></script>
	</body>
</html>




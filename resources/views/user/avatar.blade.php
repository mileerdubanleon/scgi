{{-- edit avatar --}}
<div class="panel shadow btn-shadow">
    <div class="header-profile header">
        <h2 class="title"><i class="far fa-user"></i> Editar avatar</h2>
    </div>
    <div class="inside">
        <div class="edit_avatar">
            {{-- message errors --}}
            @if($errors->has('avatar'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('avatar') }}
                </div>
            @endif

            {!! Form::open(['url' => 'account/edit/avatar', 'id' => 'form_avatar_change', 'files' => true]) !!}
                <a href="#" id="btn_avatar_edit">
                    <div class="overlay" id="avatar_change_overlay"><i class="fas fa-camera"></i></div>
                    @if(Auth::user()->avatar)
                        <img src="{{ asset('/uploads_user/' . Auth::id() . '/' . Auth::user()->avatar) }}" class="my-3 shadow" style="width: 150px; height: 150px;border: 2px solid #27aef7;" alt="Avatar">
                    @else
                        <img src="{{ asset('/static/images/default-avatar.png') }}" alt="Default Avatar">
                    @endif
                </a> 
                {!! Form::file('avatar', ['id' => 'input_file_avatar', 'accept' => 'image/*', 'class' => 'form-control']) !!}
                <div class="col-md-12 my-4">
                    {!! Form::submit('Actualizar avatar', ['class' => 'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}

            {!! Form::close() !!}
        </div>
    </div>
</div>


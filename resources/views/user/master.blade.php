<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="routeName" content="{{ Route::currentRouteName() }}">
	{{-- favicon --}}
	<link rel="icon" type="image/x-icon" href="/static/images/icon.png">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/fontawesome-free/css/all.min.css') }}">
	
	<!-- Styles -->
	<link href="{{ asset('static/css/nav/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('static/css/nav/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('static/css/profile/style.css') }}" rel="stylesheet">
</head>
	<body>

		<!-- Preloader -->
		<div class="spinner-wrapper">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
		<!-- end of preloader -->
		
		{{-- <div class="pb-5 mt-5">
            <!-- Barra de navegación -->
            @include('assets.navbar')
        </div> --}}
		

        <!-- Contenido específico de la página -->
        <div class="mx-5 mt-2 pt-5">
            @yield('content')
        </div>

		<!-- Scripts -->
		<script src="{{ asset('static/js/front/jquery.min.js') }}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
		<script src="{{ asset('static/js/front/popper.min.js') }}"></script> <!-- Popper tooltip library for Bootstrap -->
		<script src="{{ asset('static/js/front/bootstrap.min.js') }}"></script> <!-- Bootstrap framework -->
		<script src="{{ asset('static/js/front/jquery.easing.min.js') }}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
		<script src="{{ asset('static/js/front/scripts.js') }}"></script> <!-- Custom scripts -->
	</body>
</html>
@extends('user.master')

@section('title', 'Editar mi perfil')

@section('content')

<div class="row">

	{{-- edit profile --}}
	<div class="col-md-4">
		
		@include('user.avatar')

		{{-- edit profile password --}}
		<div class="panel shadow mt-5 mb-4">
			<div class="header">
				<h2 class="title"><i class="fas fa-fingerprint"></i> Cambiar contraseña</h2>
			</div>
			<div class="inside">
				{!! Form::open(['url' => '/account/edit/password']) !!}
					<div class="row">
						<div class="col-md-12">
							<label for="name">Contraseña actual: </label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
								<i class="far fa-keyboard"></i>
								</span>
								{!! Form::password('apassword', ['class' => 'form-control']) !!}
							</div>
							{{-- message errors --}}
							@if($errors->has('apassword'))
								<div class=" alert-danger" role="alert">
									{{ $errors->first('apassword') }}
								</div>
							@endif
						</div>
					</div>

					<div class="row my-2">
						<div class="col-md-12">
							<label for="name">Nueva contraseña: </label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
								<i class="far fa-keyboard"></i>
								</span>
								{!! Form::password('password', ['class' => 'form-control']) !!}
							</div>
							{{-- message errors --}}
							@if($errors->has('password'))
								<div class=" alert-danger" role="alert">
									{{ $errors->first('password') }}
								</div>
							@endif
						</div>
						
					</div>

					<div class="row mtop16">
						<div class="col-md-12">
							<label for="name">Confirmar contraseña: </label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
								<i class="far fa-keyboard"></i>
								</span>
								{!! Form::password('cpassword', ['class' => 'form-control']) !!}
							</div>
							{{-- message errors --}}
							@if($errors->has('cpassword'))
								<div class=" alert-danger" role="alert">
									{{ $errors->first('cpassword') }}
								</div>
							@endif
						</div>
					</div>

					<div class="row mtop16 pt-3">
						<div class="col-md-12">
							{!! Form::submit('Actualizar contraseña', ['class' => 'btn btn-success']) !!}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		{{-- edit profile password --}}
	</div>
	{{-- edit profile --}}

	<!-- ./wrapper -->
	<div class="col-md-8 pb-3">
		<div class="panel shadow">
			<div class="header">
				<h2 class="title"><i class="far fa-address-card"></i> Editar información</h2>
			</div>
			<div class="inside">
				{!! Form::open(['url' => '/account/edit/info']) !!}
					<div class="row">
						<div class="col-md-4">
							<label for="name">Nombre:</label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
									<i class="fas fa-keyboard"></i>
								</span>
								{!! Form::text('name', Auth::user()->name, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<label for="lastname">Apellidos: </label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
									<i class="fas fa-keyboard"></i>
								</span>
								{!! Form::text('lastname', Auth::user()->lastname, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="col-md-4">
							<label for="email">Correo electrónico: </label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
									<i class="fas fa-keyboard"></i>
								</span>
								{!! Form::text('email', Auth::user()->email, ['class' => 'form-control', 'disabled']) !!}
							</div>
						</div>
					</div>

					<div class="row my-3">
						<div class="col-md-4">
							<label for="phone">Teléfono: </label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
									<i class="fas fa-keyboard"></i>
								</span>
								{!! Form::number('phone', Auth::user()->phone, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="col-md-8">
								<label for="module" class="">Fecha de nacimiento: Año - Mes - Día</label>		
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
									<i class="far fa-keyboard" style="width: 16px; height: 24px;"></i>
								</span>
								{!! Form::number('year', $birthday[0], ['class' => 'form-control', 'min' => getUserYears()[1], 'max' => getUserYears()[0], 'required']) !!}
								{!! Form::select('month', getMonths('list', null), $birthday[1], ['class' => 'form-control']) !!}
								{!! Form::number('day', $birthday[2], ['class' => 'form-control', 'min' => 1, 'max' => 31, 'required']) !!}
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 mtop16">
								<label for="gender" class="">Género: </label>
							<div class="input-group">
								<span class="input-group-text" id="basic-addon1">
								<i class="far fa-keyboard" style="width: 16px; height: 24px;"></i>
								</span>
								{!! Form::select('gender', ['0' => 'Sin especificar', '1' => 'Hombre', '2' => 'Mujer'], Auth::user()->gender, ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>

					<div class="row mt-3">
						<div class="col-md-12">
							{!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	
</div>
@endsection



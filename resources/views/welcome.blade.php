<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Alumart</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="routeName" content="{{ Route::currentRouteName() }}">
	{{-- favicon --}}
	<link rel="icon" type="image/x-icon" href="/static/images/icon.png">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('static/css/plugins/fontawesome-free/css/all.min.css') }}">
	
	<!-- Styles -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
	<link href="{{ asset('static/css/nav/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('static/css/nav/fontawesome-all.css') }}" rel="stylesheet">
	<link href="{{ asset('static/css/nav/swiper.css') }}" rel="stylesheet">
	<link href="{{ asset('static/css/nav/magnific-popup.css') }}" rel="stylesheet">
	<link href="{{ asset('static/css/nav/styles.css') }}" rel="stylesheet">
	<link href="{{ asset('static/css/nav/swiper.css') }}" rel="stylesheet">
</head>
	<body>

		<!-- Preloader -->
		<div class="spinner-wrapper">
			<div class="spinner">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
		<!-- end of preloader -->
		
		@include('assets.navbar')
		
		{{-- home --}}
		@include('assets.home')
		@include('assets.navbarMobile')
		{{-- slider publicity --}}
		@include('assets.slider')
		{{-- services --}}
		@include('assets.services')
		{{-- about --}}
		@include('assets.about')
		{{-- store --}}
		@include('assets.store')
		{{-- contact --}}
		@include('assets.contact')
		{{-- address --}}
		@include('assets.address')
		{{-- footer --}}
		@include('assets.footer')

		<!-- Scripts -->
		<script src="{{ asset('static/js/front/jquery.min.js') }}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
		<script src="{{ asset('static/js/front/popper.min.js') }}"></script> <!-- Popper tooltip library for Bootstrap -->
		<script src="{{ asset('static/js/front/bootstrap.min.js') }}"></script> <!-- Bootstrap framework -->
		<script src="{{ asset('static/js/front/jquery.easing.min.js') }}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
		<script src="{{ asset('static/js/front/scripts.js') }}"></script> <!-- Custom scripts -->
		
	</body>
</html>